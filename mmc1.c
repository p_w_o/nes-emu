#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "mmc1.h"

enum mmc1_cpu_mapping {
	CPU_RAM,
	CPU_PPU_REGS,
	CPU_APU_REGS_0,
	CPU_PPU_DMA,
	CPU_APU_REGS_1,
	CPU_PAD_REG_0,
	CPU_PAD_REG_1,
	CPU_DUMMY,
	CPU_PRG_RAM,
	CPU_PRG_ROM_0,
	CPU_PRG_ROM_1
};

enum mmc1_ppu_mapping {
	PPU_CHR_0,
	PPU_CHR_1,
	PPU_NAMESPACE_0,
	PPU_NAMESPACE_1,
	PPU_NAMESPACE_2,
	PPU_NAMESPACE_3,
	PPU_NAMESPACE_0_MIRROR,
	PPU_NAMESPACE_1_MIRROR,
	PPU_NAMESPACE_2_MIRROR,
	PPU_NAMESPACE_3_MIRROR,
	PPU_PALETTE_RAM,
};


typedef struct {
	mapper_t mapper;
	uint8_t sr;
    uint8_t control;
    uint8_t chr_bank_0;
    uint8_t chr_bank_1;
    uint8_t prg_bank;

    uint8_t cpu_ram[2 * 1024];
    uint8_t prg_ram[8 * 1024];

    uint8_t prg_bank_count;
    uint8_t prg[512 * 1024];
    uint32_t prg_bank_0_offset;
	uint32_t prg_bank_1_offset;

    uint8_t chr_bank_count;
    uint8_t chr[256 * 1024];
    uint32_t chr_bank_0_offset;
	uint32_t chr_bank_1_offset;

	uint8_t ppu_ram[2 * 1024];
	uint8_t palette_ram[32];

    address_mapping_t cpu_mappings[CPU_PRG_ROM_1 + 1];
    address_mapping_t ppu_mappings[PPU_PALETTE_RAM + 1];
} mmc1_t;



static uint8_t mmc1_cpu_ram_read_ubyte(void *mapper, uint16_t address) {
	assert(address < 2 * 1024);
	mmc1_t *self = mapper;
	return self->cpu_ram[address];
}

static void mmc1_cpu_ram_write_ubyte(void *mapper, uint16_t address, uint8_t value) {
	assert(address < 2 * 1024);
	mmc1_t *self = mapper;
	self->cpu_ram[address] = value;
}


static uint8_t mmc1_prg_ram_read_ubyte(void *mapper, uint16_t address) {
	assert(address < 8 * 1024);
	mmc1_t *self = mapper;
	return self->prg_ram[address];
}

static void mmc1_prg_ram_write_ubyte(void *mapper, uint16_t address, uint8_t value) {
	assert(address < 8 * 1024);
	mmc1_t *self = mapper;
	self->prg_ram[address] = value;
}


static uint8_t mmc1_prg_bank_0_read_ubyte(void *mapper, uint16_t address) {
	assert(address < 16 * 1024);
	mmc1_t *self = mapper;
	return self->prg[self->prg_bank_0_offset + address];
}


static uint8_t mmc1_prg_bank_1_read_ubyte(void *mapper, uint16_t address) {
	assert(address < 16 * 1024);
	mmc1_t *self = mapper;
	return self->prg[self->prg_bank_1_offset + address];
}


static void update_prg_mappings(mmc1_t *self) {
	uint8_t bank_0;
	uint8_t bank_1;

	switch (self->control >> 2 & 0x03) {
		case 0:
		case 1:
			bank_0 = (self->prg_bank & 0x1E) % self->prg_bank_count;
			bank_1 = (bank_0 + 1) % self->prg_bank_count;
			break;
		case 2:
			bank_0 = 0;
			bank_1 = self->prg_bank % self->prg_bank_count;
			break;
		case 3:
			bank_0 = self->prg_bank % self->prg_bank_count;
			bank_1 = self->prg_bank_count - 1;
			break;
		default:
			printf("MMC1: bug in PRG bank logic\n");
			exit(-1);
	}

	self->prg_bank_0_offset = bank_0 * 16 * 1024;
	self->prg_bank_1_offset = bank_1 * 16 * 1024;
}


static void update_chr_mappings(mmc1_t *self) {
	uint8_t bank_0;
	uint8_t bank_1;

	if (self->control >> 4 & 1) {
		bank_0 = self->chr_bank_0 % self->chr_bank_count;
		bank_1 = self->chr_bank_1 % self->chr_bank_count;
	}
	else {
		bank_0 = self->chr_bank_0 & 0x1E % self->chr_bank_count;
		bank_1 = (bank_0 + 1) % self->chr_bank_count;
	}

	self->chr_bank_0_offset = bank_0 * 4 * 1024;
	self->chr_bank_1_offset = bank_1 * 4 * 1024;
}


static void update_namespace_mappings(mmc1_t *self) {
	switch (self->control & 0x03) {
		case 0:
			self->ppu_mappings[PPU_NAMESPACE_0].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_0_MIRROR].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_1].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_1_MIRROR].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_2].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_2_MIRROR].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_3].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_3_MIRROR].target_min_address = 0x0000;
			break;
		case 1:
			self->ppu_mappings[PPU_NAMESPACE_0].target_min_address = 0x0400;
			self->ppu_mappings[PPU_NAMESPACE_0_MIRROR].target_min_address = 0x0400;
			self->ppu_mappings[PPU_NAMESPACE_1].target_min_address = 0x0400;
			self->ppu_mappings[PPU_NAMESPACE_1_MIRROR].target_min_address = 0x0400;
			self->ppu_mappings[PPU_NAMESPACE_2].target_min_address = 0x0400;
			self->ppu_mappings[PPU_NAMESPACE_2_MIRROR].target_min_address = 0x0400;
			self->ppu_mappings[PPU_NAMESPACE_3].target_min_address = 0x0400;
			self->ppu_mappings[PPU_NAMESPACE_3_MIRROR].target_min_address = 0x0400;
			break;
		case 2:
			self->ppu_mappings[PPU_NAMESPACE_0].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_0_MIRROR].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_1].target_min_address = 0x0400;
			self->ppu_mappings[PPU_NAMESPACE_1_MIRROR].target_min_address = 0x0400;
			self->ppu_mappings[PPU_NAMESPACE_2].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_2_MIRROR].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_3].target_min_address = 0x0400;
			self->ppu_mappings[PPU_NAMESPACE_3_MIRROR].target_min_address = 0x0400;
			break;
		case 3:
			self->ppu_mappings[PPU_NAMESPACE_0].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_0_MIRROR].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_1].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_1_MIRROR].target_min_address = 0x0000;
			self->ppu_mappings[PPU_NAMESPACE_2].target_min_address = 0x0400;
			self->ppu_mappings[PPU_NAMESPACE_2_MIRROR].target_min_address = 0x0400;
			self->ppu_mappings[PPU_NAMESPACE_3].target_min_address = 0x0400;
			self->ppu_mappings[PPU_NAMESPACE_3_MIRROR].target_min_address = 0x0400;
			break;
	}

	uint8_t bank_0;
	uint8_t bank_1;

	if (self->control >> 4 & 1) {
		bank_0 = self->chr_bank_0 % self->chr_bank_count;
		bank_1 = self->chr_bank_1 % self->chr_bank_count;
	}
	else {
		bank_0 = self->chr_bank_0 & 0x1E % self->chr_bank_count;
		bank_1 = (bank_0 + 1) % self->chr_bank_count;
	}

	self->chr_bank_0_offset = bank_0 * 4 * 1024;
	self->chr_bank_1_offset = bank_1 * 4 * 1024;
}



static void mmc1_prg_bank_0_write_ubyte(void *mapper, uint16_t address, uint8_t value) {
	assert(address < 16 * 1024);
	mmc1_t *self = mapper;
	if (value & 0x80) {
		self->sr = 0x80;
		self->control |= 0x0C;
		update_prg_mappings(self);
		update_chr_mappings(self);
		return;
	}

	self->sr = self->sr >> 1 | ((value & 1) << 7);
	if (self->sr & 4) {
		if (address < 0x2000) {     	// Control register
			self->control = self->sr >> 3;
			update_prg_mappings(self);
			update_namespace_mappings(self);
		}
		else {	// CHR bank 0 register
			self->chr_bank_0 = self->sr >> 3;
		}
		self->sr = 0x80;
		update_chr_mappings(self);
	}
}


static void mmc1_prg_bank_1_write_ubyte(void *mapper, uint16_t address, uint8_t value) {
	assert(address < 16 * 1024);
	mmc1_t *self = mapper;
	if (value & 0x80) {
		self->sr = 0x80;
		self->control |= 0x0C;
		update_prg_mappings(self);
		update_chr_mappings(self);
		return;
	}

	self->sr = self->sr >> 1 | ((value & 1) << 7);
	if (self->sr & 4) {
		if (address < 0x2000) {	// CHR bank 1 register
			self->chr_bank_1 = self->sr >> 3;
			update_chr_mappings(self);
		}
		else {	// PRG bank register
			self->prg_bank = self->sr >> 3;
			update_prg_mappings(self);
		}

		self->sr = 0x80;
	}
}



static uint8_t mmc1_chr_bank_0_read_ubyte(void *mapper, uint16_t address) {
	assert(address < 4 * 1024);
	mmc1_t *self = mapper;
	return self->chr[self->chr_bank_0_offset + address];
}


static uint8_t mmc1_chr_bank_1_read_ubyte(void *mapper, uint16_t address) {
	assert(address < 4 * 1024);
	mmc1_t *self = mapper;
	return self->chr[self->chr_bank_1_offset + address];
}


static void mmc1_chr_bank_0_write_ubyte(void *mapper, uint16_t address, uint8_t value) {
	assert(address < 4 * 1024);
	mmc1_t *self = mapper;
	self->chr[self->chr_bank_0_offset + address] = value;
}

static void mmc1_chr_bank_1_write_ubyte(void *mapper, uint16_t address, uint8_t value) {
	assert(address < 4 * 1024);
	mmc1_t *self = mapper;
	self->chr[self->chr_bank_1_offset + address] = value;
}

static void mmc1_chr_rom_write_ubyte(void *mapper, uint16_t address, uint8_t value) {
	printf("Write to CHR ROM detected\n");
}

static uint8_t mmc1_ppu_ram_read_ubyte(void *mapper, uint16_t address) {
	assert(address < 2 * 1024);
	mmc1_t *self = mapper;
	return self->ppu_ram[address];
}

static void mmc1_ppu_ram_write_ubyte(void *mapper, uint16_t address, uint8_t value) {
	assert(address < 2 * 1024);
	mmc1_t *self = mapper;
	self->ppu_ram[address] = value;
}


static uint8_t mmc1_palette_ram_read_ubyte(void *mapper, uint16_t address) {
	assert(address < 32);
	mmc1_t *self = mapper;
	if ((address & 0x13) == 0x10)
		return self->palette_ram[address ^ 0x10];
	return self->palette_ram[address];
}

static void mmc1_palette_ram_write_ubyte(void *mapper, uint16_t address, uint8_t value) {
	assert(address < 32);
	mmc1_t *self = mapper;
	if ((address & 0x13) == 0x10) {
		printf("Writing at palette address %04X, corrected to %04X (value %02X)\n", address, address ^ 0x10, value);
		self->palette_ram[address ^ 0x10] = value & 0x3F;
	}
	else
		self->palette_ram[address] = value & 0x3F;
}

static uint8_t mmc1_dummy_read_ubyte(void *mapper, uint16_t address) {
	printf("Unsupported read from %04X\n", address);
	return 0;
}

static void mmc1_dummy_write_ubyte(void *mapper, uint16_t address, uint8_t value) {
	printf("Unsupported write of %02X to %04X\n", value, address);
}




static void init_cpu(mmc1_t *mmc1) {
	mmc1->cpu_mappings[CPU_RAM].source_min_address = 0x0000;
	mmc1->cpu_mappings[CPU_RAM].source_max_address = 0x1FFF;
	mmc1->cpu_mappings[CPU_RAM].target_min_address = 0x0000;
	mmc1->cpu_mappings[CPU_RAM].target_length = 0x0800;
	mmc1->cpu_mappings[CPU_RAM].read_byte = &mmc1_cpu_ram_read_ubyte;
	mmc1->cpu_mappings[CPU_RAM].write_byte = &mmc1_cpu_ram_write_ubyte;

	mmc1->cpu_mappings[CPU_PPU_REGS].source_min_address = 0x2000;
	mmc1->cpu_mappings[CPU_PPU_REGS].source_max_address = 0x3FFF;
	mmc1->cpu_mappings[CPU_PPU_REGS].target_min_address = 0x0000;
	mmc1->cpu_mappings[CPU_PPU_REGS].target_length = 0x0008;
	mmc1->cpu_mappings[CPU_PPU_REGS].read_byte = &ppu_read_ubyte;
	mmc1->cpu_mappings[CPU_PPU_REGS].write_byte = &ppu_write_ubyte;

	mmc1->cpu_mappings[CPU_APU_REGS_0].source_min_address = 0x4000;
	mmc1->cpu_mappings[CPU_APU_REGS_0].source_max_address = 0x4013;
	mmc1->cpu_mappings[CPU_APU_REGS_0].target_min_address = 0x0000;
	mmc1->cpu_mappings[CPU_APU_REGS_0].target_length = 0x0013;
	mmc1->cpu_mappings[CPU_APU_REGS_0].read_byte = &apu_read_ubyte;
	mmc1->cpu_mappings[CPU_APU_REGS_0].write_byte = &apu_write_ubyte;

	mmc1->cpu_mappings[CPU_PPU_DMA].source_min_address = 0x4014,
	mmc1->cpu_mappings[CPU_PPU_DMA].source_max_address = 0x4014;
	mmc1->cpu_mappings[CPU_PPU_DMA].target_min_address = 0x0008;
	mmc1->cpu_mappings[CPU_PPU_DMA].target_length = 0x0001;
	mmc1->cpu_mappings[CPU_PPU_DMA].read_byte = &ppu_read_ubyte;
	mmc1->cpu_mappings[CPU_PPU_DMA].write_byte = &ppu_write_ubyte;

	mmc1->cpu_mappings[CPU_APU_REGS_1].source_min_address = 0x4015;
	mmc1->cpu_mappings[CPU_APU_REGS_1].source_max_address = 0x4015;
	mmc1->cpu_mappings[CPU_APU_REGS_1].target_min_address = 0x0015;
	mmc1->cpu_mappings[CPU_APU_REGS_1].target_length = 0x0003;
	mmc1->cpu_mappings[CPU_APU_REGS_1].read_byte = &apu_read_ubyte;
	mmc1->cpu_mappings[CPU_APU_REGS_1].write_byte = &apu_write_ubyte;

	mmc1->cpu_mappings[CPU_PAD_REG_0].source_min_address = 0x4016;
	mmc1->cpu_mappings[CPU_PAD_REG_0].source_max_address = 0x4016;
	mmc1->cpu_mappings[CPU_PAD_REG_0].target_min_address = 0x0000;
	mmc1->cpu_mappings[CPU_PAD_REG_0].target_length = 0x0001;
	mmc1->cpu_mappings[CPU_PAD_REG_0].read_byte = &pad_read_ubyte;
	mmc1->cpu_mappings[CPU_PAD_REG_0].write_byte = &pad_write_ubyte;

	mmc1->cpu_mappings[CPU_PAD_REG_1].source_min_address = 0x4017;
	mmc1->cpu_mappings[CPU_PAD_REG_1].source_max_address = 0x4017;
	mmc1->cpu_mappings[CPU_PAD_REG_1].target_min_address = 0x0017;
	mmc1->cpu_mappings[CPU_PAD_REG_1].target_length = 0x0001;
	mmc1->cpu_mappings[CPU_PAD_REG_1].read_byte = &pad_read_ubyte;
	mmc1->cpu_mappings[CPU_PAD_REG_1].write_byte = &apu_write_ubyte;

	mmc1->cpu_mappings[CPU_DUMMY].source_min_address = 0x4018;
	mmc1->cpu_mappings[CPU_DUMMY].source_max_address = 0x5FFF;
	mmc1->cpu_mappings[CPU_DUMMY].target_min_address = 0x4018;
	mmc1->cpu_mappings[CPU_DUMMY].target_length = 0x2000;
	mmc1->cpu_mappings[CPU_DUMMY].read_byte = &mmc1_dummy_read_ubyte;
	mmc1->cpu_mappings[CPU_DUMMY].write_byte = &mmc1_dummy_write_ubyte;

	mmc1->cpu_mappings[CPU_PRG_RAM].source_min_address = 0x6000;
	mmc1->cpu_mappings[CPU_PRG_RAM].source_max_address = 0x7FFF;
	mmc1->cpu_mappings[CPU_PRG_RAM].target_min_address = 0x0000;
	mmc1->cpu_mappings[CPU_PRG_RAM].target_length = 0x2000;
	mmc1->cpu_mappings[CPU_PRG_RAM].read_byte = &mmc1_prg_ram_read_ubyte;
	mmc1->cpu_mappings[CPU_PRG_RAM].write_byte = &mmc1_prg_ram_write_ubyte;

	mmc1->cpu_mappings[CPU_PRG_ROM_0].source_min_address = 0x8000;
	mmc1->cpu_mappings[CPU_PRG_ROM_0].source_max_address = 0xBFFF;
	mmc1->cpu_mappings[CPU_PRG_ROM_0].target_min_address = 0x0000;
	mmc1->cpu_mappings[CPU_PRG_ROM_0].target_length = 0x4000;
	mmc1->cpu_mappings[CPU_PRG_ROM_0].read_byte = &mmc1_prg_bank_0_read_ubyte;
	mmc1->cpu_mappings[CPU_PRG_ROM_0].write_byte = &mmc1_prg_bank_0_write_ubyte;

	mmc1->cpu_mappings[CPU_PRG_ROM_1].source_min_address = 0xC000;
	mmc1->cpu_mappings[CPU_PRG_ROM_1].source_max_address = 0xFFFF;
	mmc1->cpu_mappings[CPU_PRG_ROM_1].target_min_address = 0x0000;
	mmc1->cpu_mappings[CPU_PRG_ROM_1].target_length = 0x4000;
	mmc1->cpu_mappings[CPU_PRG_ROM_1].read_byte = &mmc1_prg_bank_1_read_ubyte;
	mmc1->cpu_mappings[CPU_PRG_ROM_1].write_byte = &mmc1_prg_bank_1_write_ubyte;

	mmc1->mapper.cpu.super.mapper = mmc1;
	mmc1->mapper.cpu.super.mapping_count = CPU_PRG_ROM_1 + 1;
	mmc1->mapper.cpu.super.address_mappings = &mmc1->cpu_mappings[0];
	cpu_init(&mmc1->mapper.cpu);
}


static void init_ppu(mmc1_t *mmc1) {
	mmc1->ppu_mappings[PPU_CHR_0].source_min_address = 0x0000;
	mmc1->ppu_mappings[PPU_CHR_0].source_max_address = 0x0FFF;
	mmc1->ppu_mappings[PPU_CHR_0].target_min_address = 0x0000;
	mmc1->ppu_mappings[PPU_CHR_0].target_length = 0x1000;
	mmc1->ppu_mappings[PPU_CHR_0].read_byte = &mmc1_chr_bank_0_read_ubyte;
	mmc1->ppu_mappings[PPU_CHR_0].write_byte = &mmc1_chr_bank_0_write_ubyte;

	mmc1->ppu_mappings[PPU_CHR_1].source_min_address = 0x1000;
	mmc1->ppu_mappings[PPU_CHR_1].source_max_address = 0x1FFF;
	mmc1->ppu_mappings[PPU_CHR_1].target_min_address = 0x0000;
	mmc1->ppu_mappings[PPU_CHR_1].target_length = 0x1000;
	mmc1->ppu_mappings[PPU_CHR_1].read_byte = &mmc1_chr_bank_1_read_ubyte;
	mmc1->ppu_mappings[PPU_CHR_1].write_byte = &mmc1_chr_bank_1_write_ubyte;

	mmc1->ppu_mappings[PPU_NAMESPACE_0].source_min_address = 0x2000;
	mmc1->ppu_mappings[PPU_NAMESPACE_0].source_max_address = 0x23FF;
	mmc1->ppu_mappings[PPU_NAMESPACE_0].target_min_address = 0x0000;
	mmc1->ppu_mappings[PPU_NAMESPACE_0].target_length = 0x0400;
	mmc1->ppu_mappings[PPU_NAMESPACE_0].read_byte = &mmc1_ppu_ram_read_ubyte;
	mmc1->ppu_mappings[PPU_NAMESPACE_0].write_byte = &mmc1_ppu_ram_write_ubyte;

	mmc1->ppu_mappings[PPU_NAMESPACE_1].source_min_address = 0x2400;
	mmc1->ppu_mappings[PPU_NAMESPACE_1].source_max_address = 0x27FF;
	mmc1->ppu_mappings[PPU_NAMESPACE_1].target_min_address = 0x0400;
	mmc1->ppu_mappings[PPU_NAMESPACE_1].target_length = 0x0400;
	mmc1->ppu_mappings[PPU_NAMESPACE_1].read_byte = &mmc1_ppu_ram_read_ubyte;
	mmc1->ppu_mappings[PPU_NAMESPACE_1].write_byte = &mmc1_ppu_ram_write_ubyte;

	mmc1->ppu_mappings[PPU_NAMESPACE_2].source_min_address = 0x2800;
	mmc1->ppu_mappings[PPU_NAMESPACE_2].source_max_address = 0x2BFF;
	mmc1->ppu_mappings[PPU_NAMESPACE_2].target_min_address = 0x0000;
	mmc1->ppu_mappings[PPU_NAMESPACE_2].target_length = 0x0400;
	mmc1->ppu_mappings[PPU_NAMESPACE_2].read_byte = &mmc1_ppu_ram_read_ubyte;
	mmc1->ppu_mappings[PPU_NAMESPACE_2].write_byte = &mmc1_ppu_ram_write_ubyte;

	mmc1->ppu_mappings[PPU_NAMESPACE_3].source_min_address = 0x2C00;
	mmc1->ppu_mappings[PPU_NAMESPACE_3].source_max_address = 0x2FFF;
	mmc1->ppu_mappings[PPU_NAMESPACE_3].target_min_address = 0x0400;
	mmc1->ppu_mappings[PPU_NAMESPACE_3].target_length = 0x0400;
	mmc1->ppu_mappings[PPU_NAMESPACE_3].read_byte = &mmc1_ppu_ram_read_ubyte;
	mmc1->ppu_mappings[PPU_NAMESPACE_3].write_byte = &mmc1_ppu_ram_write_ubyte;

	mmc1->ppu_mappings[PPU_NAMESPACE_0_MIRROR].source_min_address = 0x3000;
	mmc1->ppu_mappings[PPU_NAMESPACE_0_MIRROR].source_max_address = 0x33FF;
	mmc1->ppu_mappings[PPU_NAMESPACE_0_MIRROR].target_min_address = 0x0000;
	mmc1->ppu_mappings[PPU_NAMESPACE_0_MIRROR].target_length = 0x0400;
	mmc1->ppu_mappings[PPU_NAMESPACE_0_MIRROR].read_byte = &mmc1_ppu_ram_read_ubyte;
	mmc1->ppu_mappings[PPU_NAMESPACE_0_MIRROR].write_byte = &mmc1_ppu_ram_write_ubyte;

	mmc1->ppu_mappings[PPU_NAMESPACE_1_MIRROR].source_min_address = 0x3400;
	mmc1->ppu_mappings[PPU_NAMESPACE_1_MIRROR].source_max_address = 0x37FF;
	mmc1->ppu_mappings[PPU_NAMESPACE_1_MIRROR].target_min_address = 0x0400;
	mmc1->ppu_mappings[PPU_NAMESPACE_1_MIRROR].target_length = 0x0400;
	mmc1->ppu_mappings[PPU_NAMESPACE_1_MIRROR].read_byte = &mmc1_ppu_ram_read_ubyte;
	mmc1->ppu_mappings[PPU_NAMESPACE_1_MIRROR].write_byte = &mmc1_ppu_ram_write_ubyte;

	mmc1->ppu_mappings[PPU_NAMESPACE_2_MIRROR].source_min_address = 0x3800;
	mmc1->ppu_mappings[PPU_NAMESPACE_2_MIRROR].source_max_address = 0x3BFF;
	mmc1->ppu_mappings[PPU_NAMESPACE_2_MIRROR].target_min_address = 0x0000;
	mmc1->ppu_mappings[PPU_NAMESPACE_2_MIRROR].target_length = 0x0400;
	mmc1->ppu_mappings[PPU_NAMESPACE_2_MIRROR].read_byte = &mmc1_ppu_ram_read_ubyte;
	mmc1->ppu_mappings[PPU_NAMESPACE_2_MIRROR].write_byte = &mmc1_ppu_ram_write_ubyte;

	mmc1->ppu_mappings[PPU_NAMESPACE_3_MIRROR].source_min_address = 0x3C00;
	mmc1->ppu_mappings[PPU_NAMESPACE_3_MIRROR].source_max_address = 0x3EFF;
	mmc1->ppu_mappings[PPU_NAMESPACE_3_MIRROR].target_min_address = 0x0400;
	mmc1->ppu_mappings[PPU_NAMESPACE_3_MIRROR].target_length = 0x0400;
	mmc1->ppu_mappings[PPU_NAMESPACE_3_MIRROR].read_byte = &mmc1_ppu_ram_read_ubyte;
	mmc1->ppu_mappings[PPU_NAMESPACE_3_MIRROR].write_byte = &mmc1_ppu_ram_write_ubyte;

	mmc1->ppu_mappings[PPU_PALETTE_RAM].source_min_address = 0x3F00;
	mmc1->ppu_mappings[PPU_PALETTE_RAM].source_max_address = 0x3FFF;
	mmc1->ppu_mappings[PPU_PALETTE_RAM].target_min_address = 0x0000;
	mmc1->ppu_mappings[PPU_PALETTE_RAM].target_length = 0x0020;
	mmc1->ppu_mappings[PPU_PALETTE_RAM].read_byte = &mmc1_palette_ram_read_ubyte;
	mmc1->ppu_mappings[PPU_PALETTE_RAM].write_byte = &mmc1_palette_ram_write_ubyte;

	mmc1->mapper.ppu.super.mapper = mmc1;
	mmc1->mapper.ppu.super.mapping_count = PPU_PALETTE_RAM + 1;
	mmc1->mapper.ppu.super.address_mappings = &mmc1->ppu_mappings[0];
	ppu_init(&mmc1->mapper.ppu);
}

static void init_apu(mmc1_t *mmc1) {
	mmc1->mapper.apu.super.mapper = mmc1;
	mmc1->mapper.apu.super.mapping_count = 0;
	apu_init(&mmc1->mapper.apu);
}


static void init_dma(mmc1_t *mmc1) {
	mmc1->mapper.dma.super.mapper = mmc1;
	mmc1->mapper.dma.super.mapping_count = 0;
	dma_init(&mmc1->mapper.dma);
}

static void init_pad(mmc1_t *mmc1) {
	mmc1->mapper.pad.super.mapper = mmc1;
	mmc1->mapper.pad.super.mapping_count = 0;
	pad_init(&mmc1->mapper.pad);
}


mapper_t *mmc1_create_mapper(rom_t *rom) {
	mmc1_t *mmc1 = calloc(1, sizeof *mmc1);
	if (!mmc1) {
		printf("Allocating memory FAILED!\n");
		exit(-1);
	}

	mmc1->prg_bank_count = rom->header.prg_rom_size;
	mmc1->sr = 0x80;
	mmc1->control = 0x0C;
	update_prg_mappings(mmc1);

	memcpy(&mmc1->prg, rom->prg_rom, rom->header.prg_rom_size * 16 * 1024);

	if (rom->header.chr_rom_size) {
		mmc1->chr_bank_count = rom->header.chr_rom_size * 2;
		memcpy(&mmc1->chr, rom->chr_rom, rom->header.chr_rom_size * 8 * 1024);
		mmc1->ppu_mappings[0].write_byte = &mmc1_chr_rom_write_ubyte;
		mmc1->ppu_mappings[1].write_byte = &mmc1_chr_rom_write_ubyte;
	}
	else {	// No CHR ROM, so 8 kB of CHR RAM instead...
		mmc1->chr_bank_count = 2;
	}
	update_chr_mappings(mmc1);

	init_cpu(mmc1);
	init_ppu(mmc1);
	init_apu(mmc1);
	init_dma(mmc1);
	init_pad(mmc1);

	return &mmc1->mapper;
}
