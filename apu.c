#include <assert.h>
#include <stdint.h>

#include "apu.h"

/*
 *  Pulse 1 channel (write)
 * $4000 	DDLC NNNN 	Duty, loop envelope/disable length counter, constant volume, envelope period/volume
 * $4001 	EPPP NSSS 	Sweep unit: enabled, period, negative, shift count
 * $4002 	LLLL LLLL 	Timer low
 * $4003 	LLLL LHHH 	Length counter load, timer high (also resets duty and starts envelope)
 *
 *  Pulse 2 channel (write)
 * $4004 	DDLC NNNN 	Duty, loop envelope/disable length counter, constant volume, envelope period/volume
 * $4005 	EPPP NSSS 	Sweep unit: enabled, period, negative, shift count
 * $4006 	LLLL LLLL 	Timer low
 * $4007 	LLLL LHHH 	Length counter load, timer high (also resets duty and starts envelope)
 *
 *  Triangle channel (write)
 * $4008 	CRRR RRRR 	Length counter disable/linear counter control, linear counter reload value
 * $400A 	LLLL LLLL 	Timer low
 * $400B 	LLLL LHHH 	Length counter load, timer high (also reloads linear counter)
 *
 *  Noise channel (write)
 * $400C 	--LC NNNN 	Loop envelope/disable length counter, constant volume, envelope period/volume
 * $400E 	L--- PPPP 	Loop noise, noise period
 * $400F 	LLLL L--- 	Length counter load (also starts envelope)
 *
 *  DMC channel (write)
 * $4010 	IL-- FFFF 	IRQ enable, loop sample, frequency index
 * $4011 	-DDD DDDD 	Direct load
 * $4012 	AAAA AAAA 	Sample address %11AAAAAA.AA000000
 * $4013 	LLLL LLLL 	Sample length %0000LLLL.LLLL0001
 *
 * $4015 	---D NT21 	Control: DMC enable, length counter enables: noise, triangle, pulse 2, pulse 1 (write)
 * $4015 	IF-D NT21 	Status: DMC interrupt, frame interrupt, length counter status: noise, triangle, pulse 2, pulse 1 (read)
 * $4017 	SD-- ---- 	Frame counter: 5-frame sequence, disable frame interrupt (write)
 */
enum registers {
	PULSE_1_REG_1,		// 0x4000
	PULSE_1_REG_2,		// 0x4001
	PULSE_1_REG_3,		// 0x4002
	PULSE_1_REG_4,		// 0x4003

	PULSE_2_REG_1,		// 0x4004
	PULSE_2_REG_2,		// 0x4005
	PULSE_2_REG_3,		// 0x4006
	PULSE_2_REG_4,		// 0x4007

	TRIANGLE_REG_1,		// 0x4008
	NON_APU_1,			// 0x4009
	TRIANGLE_REG_2,		// 0x400A
	TRIANGLE_REG_3,		// 0x400B

	NOISE_REG_1,		// 0x400C
	NON_APU_2,			// 0x400D
	NOISE_REG_2,		// 0x400E
	NOISE_REG_3,		// 0x400F

	DMC_REG_1,			// 0x4010
	DMC_REG_2,			// 0x4011
	DMC_REG_3,			// 0x4012
	DMC_REG_4,			// 0x4013

	NON_APU_3,			// 0x4014

	CONTROL_REG,		// 0x4015

	NON_APU_4,			// 0x4016

	FRAME_COUNTER		// 0x4017
};





void apu_write_ubyte(void *mapper, uint16_t address, uint8_t value) {
	// TODO: implement write;
	printf("Unimplemented APU register write at local address %02X\n", address);
}


uint8_t apu_read_ubyte(void *mapper, uint16_t address) {
	// TODO: implement read;
	printf("Unimplemented APU register read from local address %02X\n", address);
	return 0;
}


void apu_init(apu_t *apu) {
	apu->super.name = "APU";
}
