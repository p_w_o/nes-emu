#ifndef PPU_H
#define PPU_H

#include "component.h"

typedef struct {
	component_t super;

	uint16_t v;		// Current VRAM address
	uint16_t t;		// Temp VRAM address
	uint8_t x;		// Fine scroll
	uint8_t w;		// Write toggle (first or second byte)

	uint8_t reg_ppuctrl;
	uint8_t reg_ppumask;
	uint8_t reg_ppustatus;
	uint8_t reg_oamaddr;
	uint8_t reg_ppudata;

	uint64_t frame;
	uint16_t scan_line;
	uint16_t scan_line_cycle;

	uint8_t vblank_set;

	uint8_t gen_latch;

	uint8_t bg_tile_low_latch;
	uint16_t bg_tile_low_shift;

	uint8_t bg_tile_high_latch;
	uint16_t bg_tile_high_shift;

	uint8_t at;
	uint8_t at_low_latch;
	uint8_t at_high_latch;
	uint8_t at_low_shift;
	uint8_t at_high_shift;

	uint8_t oam[256];
	uint8_t oam_secondary[32];
	uint8_t oam_current[32];
	uint8_t sprite_0_visible;
	uint8_t picture[256 * 240];
} ppu_t;


void ppu_init(ppu_t *ppu);

int ppu_emulate(ppu_t *ppu);


uint8_t ppu_read_ubyte(void *mapper, uint16_t address);

void ppu_write_ubyte(void *mapper, uint16_t address, uint8_t value);


#endif /* PPU_H */
