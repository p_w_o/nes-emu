#ifndef APU_H_
#define APU_H_

#include "component.h"


typedef struct {
	component_t super;
} apu_t;



void apu_init(apu_t *apu);

void apu_emulate(apu_t *apu);


uint8_t apu_read_ubyte(void *mapper, uint16_t address);

void apu_write_ubyte(void *mapper, uint16_t address, uint8_t value);


#endif /* APU_H_ */
