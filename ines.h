#ifndef INES_H
#define INES_H

#include <stdint.h>

typedef struct {
	uint8_t magic[4];
	uint8_t prg_rom_size;
	uint8_t chr_rom_size;
	uint8_t flags6;
	uint8_t flags7;
	uint8_t prg_ram_size;
	uint8_t flags9;
	uint8_t flags10;
	uint8_t zero[5];
} rom_header_t;

typedef struct {
	rom_header_t header;
	uint8_t trainer[512];
	uint8_t* prg_rom;
	uint8_t* chr_rom;
	uint8_t inst_rom[8 * 1024];
	uint8_t prom[32];
} rom_t;


rom_t *read_rom(FILE *fp);
void print_rom_header(rom_t *rom);

#endif /* INES_H */
