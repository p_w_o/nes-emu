#include <stdio.h>
#include "utils.h"

void print_ubyte_as_binary(uint8_t data) {
	for (int i = 7; i >= 0; --i)
		printf("%d", data >> i & 1);
}

void print_ushort_as_binary(uint16_t data) {
	for (int i = 15; i >= 0; --i)
		printf("%d", data >> i & 1);
}

void dump_as_hex(uint8_t *data, size_t size, uint16_t address_offset) {
	for (int i = 0; i < size; ++i) {
		if (i % 8 == 0)
			printf("  0x%-4X: ", i + address_offset);
		
		printf("0x%02X ", data[i]);
		if ((i+1) % 32 == 0)
			printf("\n");
	}
}
