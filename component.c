#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "component.h"


typedef struct {
    component_t super;
    uint16_t length;
    uint8_t *data;
} memory_component_t;


static address_mapping_t *find_mapping(component_t component, uint16_t address) {
    for (int i = 0; i < component.mapping_count; ++i) {
        address_mapping_t *mapping = &component.address_mappings[i];
        if (address < mapping->source_min_address)
            continue;
        if (address > mapping->source_max_address)
            continue;

        return mapping;
    }

    printf("Could not access address 0x%04X of %s: no mapping found\n", address, component.name);
    exit(-2);
}

static uint16_t calculate_local_address(address_mapping_t *mapping, uint16_t address) {
    return mapping->target_min_address + (address - mapping->source_min_address) % mapping->target_length;
}


uint8_t read_ubyte(component_t component, uint16_t address) {
    address_mapping_t *mapping = find_mapping(component, address);
    uint16_t local_address = calculate_local_address(mapping, address);
    return mapping->read_byte(component.mapper, local_address);
}

uint16_t read_ushort(component_t component, uint16_t address) {
    return read_ubyte(component, address) + read_ubyte(component, address + 1) * 256;
}


void write_ubyte(component_t component, uint16_t address, uint8_t value) {
    address_mapping_t *mapping = find_mapping(component, address);
    uint16_t local_address = calculate_local_address(mapping, address);
    mapping->write_byte(component.mapper, local_address, value);
}


void write_ushort(component_t component, uint16_t address, uint16_t value) {
    write_ubyte(component, address, (uint8_t)value);
    write_ubyte(component, address + 1, (uint8_t)(value >> 8));
}
