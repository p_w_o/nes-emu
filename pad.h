#ifndef PAD_H_
#define PAD_H_

#include "component.h"

enum PadButtons { PAD_A, PAD_B, PAD_SELECT, PAD_START, PAD_UP, PAD_DOWN, PAD_LEFT, PAD_RIGHT };


typedef struct {
	component_t super;

	uint8_t reg_in;
	uint8_t reg_out_0;
	uint8_t reg_out_1;

	uint8_t pad_0_state;
	uint8_t pad_1_state;
} pad_t;


void pad_init(pad_t *pad);


uint8_t pad_read_ubyte(void *mapper, uint16_t address);

void pad_write_ubyte(void *mapper, uint16_t address, uint8_t value);


#endif /* PAD_H_ */
