#ifndef DMA_H_
#define DMA_H_

#include <stdint.h>
#include "component.h"


typedef struct {
	component_t super;
	uint16_t src;
	uint8_t target;
	uint16_t count;
} dma_t;


void dma_init(dma_t *dma);

uint8_t dma_emulate(dma_t *dma);


#endif /* DMA_H_ */
