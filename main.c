#include <cpu.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "SDL.h"

#include "ines.h"
#include "component.h"
#include "mapper.h"
#include "mmc1.h"
#include "apu.h"
#include "dma.h"
#include "pad.h"
#include "ppu.h"
#include "utils.h"

#define SCALE 3
#define PAD_DEAD_ZONE 10240

typedef struct {
	SDL_atomic_t done;
	SDL_sem *semaphore;
	uint8_t picture[256 * 240];
	int result;
} ui_t;


typedef struct {
	uint8_t *state;
	SDL_GameController *controller;
} controller_t;


static int emulate_frame(mapper_t *mapper, int cpu_skip) {
	int cycle = 0;
	int stop;
	do {
		if (cycle % 3 == 0) {
			if (--cpu_skip == 0) {
				cpu_skip = dma_emulate(&mapper->dma);
				if (cpu_skip == 0)
					cpu_skip = cpu_emulate(&mapper->cpu);
				else
					mapper->cpu.cycle += cpu_skip;
			}
		}

		stop = ppu_emulate(&mapper->ppu);

		++cycle;
	} while (!stop);

	return cpu_skip;
}


static void set_color(SDL_Renderer *renderer, uint8_t color) {
	switch (color) {
		case 0x00:	SDL_SetRenderDrawColor(renderer, 0x65, 0x65, 0x65, 0xFF); break;
		case 0x01:	SDL_SetRenderDrawColor(renderer, 0x00, 0x2B, 0x9B, 0xFF); break;
		case 0x02:	SDL_SetRenderDrawColor(renderer, 0x11, 0x0E, 0xC0, 0xFF); break;
		case 0x03:	SDL_SetRenderDrawColor(renderer, 0x3F, 0x00, 0xBC, 0xFF); break;
		case 0x04:	SDL_SetRenderDrawColor(renderer, 0x66, 0x00, 0x8F, 0xFF); break;
		case 0x05:	SDL_SetRenderDrawColor(renderer, 0x7B, 0x00, 0x45, 0xFF); break;
		case 0x06:	SDL_SetRenderDrawColor(renderer, 0x79, 0x01, 0x00, 0xFF); break;
		case 0x07:	SDL_SetRenderDrawColor(renderer, 0x60, 0x1C, 0x00, 0xFF); break;
		case 0x08:	SDL_SetRenderDrawColor(renderer, 0x36, 0x38, 0x00, 0xFF); break;
		case 0x09:	SDL_SetRenderDrawColor(renderer, 0x08, 0x4F, 0x00, 0xFF); break;
		case 0x0A:	SDL_SetRenderDrawColor(renderer, 0x00, 0x5A, 0x00, 0xFF); break;
		case 0x0B:	SDL_SetRenderDrawColor(renderer, 0x00, 0x57, 0x02, 0xFF); break;
		case 0x0C:	SDL_SetRenderDrawColor(renderer, 0x00, 0x45, 0x55, 0xFF); break;
		case 0x0D:	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF); break;
		case 0x0E:	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF); break;
		case 0x0F:	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF); break;
		case 0x10:	SDL_SetRenderDrawColor(renderer, 0xAE, 0xAE, 0xAE, 0xFF); break;
		case 0x11:	SDL_SetRenderDrawColor(renderer, 0x07, 0x61, 0xF5, 0xFF); break;
		case 0x12:	SDL_SetRenderDrawColor(renderer, 0x3E, 0x3B, 0xFF, 0xFF); break;
		case 0x13:	SDL_SetRenderDrawColor(renderer, 0x7C, 0x1D, 0xFF, 0xFF); break;
		case 0x14:	SDL_SetRenderDrawColor(renderer, 0xAF, 0x0E, 0xE5, 0xFF); break;
		case 0x15:	SDL_SetRenderDrawColor(renderer, 0xCB, 0x13, 0x83, 0xFF); break;
		case 0x16:	SDL_SetRenderDrawColor(renderer, 0xC8, 0x2A, 0x15, 0xFF); break;
		case 0x17:	SDL_SetRenderDrawColor(renderer, 0xA7, 0x4D, 0x00, 0xFF); break;
		case 0x18:	SDL_SetRenderDrawColor(renderer, 0x6F, 0x72, 0x00, 0xFF); break;
		case 0x19:	SDL_SetRenderDrawColor(renderer, 0x32, 0x91, 0x00, 0xFF); break;
		case 0x1A:	SDL_SetRenderDrawColor(renderer, 0x00, 0x9F, 0x00, 0xFF); break;
		case 0x1B:	SDL_SetRenderDrawColor(renderer, 0x00, 0x9B, 0x2A, 0xFF); break;
		case 0x1C:	SDL_SetRenderDrawColor(renderer, 0x00, 0x84, 0x98, 0xFF); break;
		case 0x1D:	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF); break;
		case 0x1E:	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF); break;
		case 0x1F:	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF); break;
		case 0x20:	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF); break;
		case 0x21:	SDL_SetRenderDrawColor(renderer, 0x56, 0xB1, 0xFF, 0xFF); break;
		case 0x22:	SDL_SetRenderDrawColor(renderer, 0x8E, 0x8B, 0xFF, 0xFF); break;
		case 0x23:	SDL_SetRenderDrawColor(renderer, 0xCC, 0x6C, 0xFF, 0xFF); break;
		case 0x24:	SDL_SetRenderDrawColor(renderer, 0xFF, 0x5D, 0xFF, 0xFF); break;
		case 0x25:	SDL_SetRenderDrawColor(renderer, 0xFF, 0x62, 0xD4, 0xFF); break;
		case 0x26:	SDL_SetRenderDrawColor(renderer, 0xFF, 0x79, 0x64, 0xFF); break;
		case 0x27:	SDL_SetRenderDrawColor(renderer, 0xF8, 0x9D, 0x06, 0xFF); break;
		case 0x28:	SDL_SetRenderDrawColor(renderer, 0xC0, 0xC3, 0x00, 0xFF); break;
		case 0x29:	SDL_SetRenderDrawColor(renderer, 0x81, 0xE2, 0x00, 0xFF); break;
		case 0x2A:	SDL_SetRenderDrawColor(renderer, 0x4D, 0xF1, 0x16, 0xFF); break;
		case 0x2B:	SDL_SetRenderDrawColor(renderer, 0x30, 0xEC, 0x7A, 0xFF); break;
		case 0x2C:	SDL_SetRenderDrawColor(renderer, 0x34, 0xD5, 0xEA, 0xFF); break;
		case 0x2D:	SDL_SetRenderDrawColor(renderer, 0x4E, 0x4E, 0x4E, 0xFF); break;
		case 0x2E:	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF); break;
		case 0x2F:	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF); break;
		case 0x30:	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF); break;
		case 0x31:	SDL_SetRenderDrawColor(renderer, 0xBA, 0xDF, 0xFF, 0xFF); break;
		case 0x32:	SDL_SetRenderDrawColor(renderer, 0xD1, 0xD0, 0xFF, 0xFF); break;
		case 0x33:	SDL_SetRenderDrawColor(renderer, 0xEB, 0xC3, 0xFF, 0xFF); break;
		case 0x34:	SDL_SetRenderDrawColor(renderer, 0xFF, 0xBD, 0xFF, 0xFF); break;
		case 0x35:	SDL_SetRenderDrawColor(renderer, 0xFF, 0xBF, 0xEE, 0xFF); break;
		case 0x36:	SDL_SetRenderDrawColor(renderer, 0xFF, 0xC8, 0xC0, 0xFF); break;
		case 0x37:	SDL_SetRenderDrawColor(renderer, 0xFC, 0xD7, 0x99, 0xFF); break;
		case 0x38:	SDL_SetRenderDrawColor(renderer, 0xE5, 0xE7, 0x84, 0xFF); break;
		case 0x39:	SDL_SetRenderDrawColor(renderer, 0xCC, 0xF3, 0x87, 0xFF); break;
		case 0x3A:	SDL_SetRenderDrawColor(renderer, 0xB6, 0xF9, 0xA0, 0xFF); break;
		case 0x3B:	SDL_SetRenderDrawColor(renderer, 0xAA, 0xF8, 0xC9, 0xFF); break;
		case 0x3C:	SDL_SetRenderDrawColor(renderer, 0xAC, 0xEE, 0xF7, 0xFF); break;
		case 0x3D:	SDL_SetRenderDrawColor(renderer, 0xB7, 0xB7, 0xB7, 0xFF); break;
		case 0x3E:	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF); break;
		case 0x3F:	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF); break;
		default:
			SDL_SetRenderDrawColor(renderer, 255, 192, 203, 255);
			printf("Invalid color: %u\n", color);
			break;
	}
}


static void draw_frame(SDL_Renderer *renderer, uint8_t *picture) {
	for (int y = 0; y < 240; ++y) {
		for (int x = 0; x < 256; ++x) {
			set_color(renderer, picture[y * 256 + x]);
			struct SDL_Rect rect = { .h = SCALE, .w = SCALE, .x = x * SCALE, .y = y *SCALE };
			SDL_RenderFillRect(renderer, &rect);
		}
	}
	SDL_RenderPresent(renderer);
}


static int render_thread(void *arg) {
	ui_t *ui = arg;

	SDL_Window *window = SDL_CreateWindow("nes-emu", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 256 * SCALE, 240 * SCALE, SDL_WINDOW_SHOWN);
	if (window == NULL) {
		printf("Could not create window: %s\n", SDL_GetError());
		exit(1);
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
	if (renderer == NULL) {
		printf("Could not create renderer: %s\n", SDL_GetError());
		exit(1);
	}

	while (!SDL_AtomicGet(&ui->done)) {
        SDL_SemWait(ui->semaphore);
        draw_frame(renderer, ui->picture);
	}

	SDL_DestroyWindow(window);
	return 0;
}


static void init_controllers(mapper_t *mapper, controller_t *controllers) {
	int count = SDL_NumJoysticks();
	if (count > 2)
		count = 2;

	if (count >= 1 && controllers[0].controller == NULL) {
		if ((controllers[0].controller = SDL_GameControllerOpen(0))) {
			printf("Pad 1 connected (%s)\n", SDL_GameControllerName(controllers[0].controller));
			controllers[0].state = &mapper->pad.pad_0_state;
		}
		else {
			printf("Could not connect pad 1: %s\n", SDL_GetError());
		}
	}

	if (count >= 2 && controllers[1].controller == NULL) {
		if ((controllers[1].controller = SDL_GameControllerOpen(1))) {
			printf("Pad 2 connected (%s)\n", SDL_GameControllerName(controllers[1].controller));
			controllers[1].state = &mapper->pad.pad_1_state;
		}
		else {
			printf("Could not connect pad 1: %s\n", SDL_GetError());
		}
	}
}

static void destruct_controller(controller_t *controllers, uint8_t id) {
	if (id > 2)
		return;

	SDL_GameControllerClose(controllers[id].controller);
	controllers[id].controller = NULL;
	controllers[id].state = NULL;
}


static void set_bit(uint8_t *target, int bit, int value) {
	if (value)
		*target |= 1 << bit;
	else
		*target &= ~(1 << bit);
}


static void update_controller_state(controller_t *controllers, SDL_ControllerButtonEvent *event) {
	if (event->which > 2)
		return;

	switch (event->button) {
		case SDL_CONTROLLER_BUTTON_A:
		case SDL_CONTROLLER_BUTTON_Y:
			set_bit(controllers[event->which].state, PAD_A, event->state == SDL_PRESSED);
			break;
		case SDL_CONTROLLER_BUTTON_B:
		case SDL_CONTROLLER_BUTTON_X:
			set_bit(controllers[event->which].state, PAD_B, event->state == SDL_PRESSED);
			break;
		case SDL_CONTROLLER_BUTTON_BACK:
			set_bit(controllers[event->which].state, PAD_SELECT, event->state == SDL_PRESSED);
			break;
		case SDL_CONTROLLER_BUTTON_START:
			set_bit(controllers[event->which].state, PAD_START, event->state == SDL_PRESSED);
			break;
		case SDL_CONTROLLER_BUTTON_DPAD_UP:
			set_bit(controllers[event->which].state, PAD_UP, event->state == SDL_PRESSED);
			break;
		case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
			set_bit(controllers[event->which].state, PAD_DOWN, event->state == SDL_PRESSED);
			break;
		case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
			set_bit(controllers[event->which].state, PAD_LEFT, event->state == SDL_PRESSED);
			break;
		case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
			set_bit(controllers[event->which].state, PAD_RIGHT, event->state == SDL_PRESSED);
			break;
		default:
			break;
	}
}

static void update_controller_axis(controller_t *controllers, SDL_ControllerAxisEvent *event) {
	if (event->which > 2)
		return;

	switch (event->axis) {
		case SDL_CONTROLLER_AXIS_LEFTX:
			set_bit(controllers[event->which].state, PAD_LEFT, event->value < -PAD_DEAD_ZONE);
			set_bit(controllers[event->which].state, PAD_RIGHT, event->value > PAD_DEAD_ZONE);
			break;
		case SDL_CONTROLLER_AXIS_LEFTY:
			set_bit(controllers[event->which].state, PAD_UP, event->value < -PAD_DEAD_ZONE);
			set_bit(controllers[event->which].state, PAD_DOWN, event->value > PAD_DEAD_ZONE);
			break;
	}
}


static void update_controller_state_kb(controller_t *controllers, SDL_KeyboardEvent *event) {
	switch (event->keysym.sym) {
		case SDLK_LALT:
		case SDLK_SPACE:
		case SDLK_x:
			set_bit(controllers[0].state, PAD_A, event->state == SDL_PRESSED);
			break;
		case SDLK_LCTRL:
		case SDLK_z:
			set_bit(controllers[0].state, PAD_B, event->state == SDL_PRESSED);
			break;
		case SDLK_LSHIFT:
			set_bit(controllers[0].state, PAD_SELECT, event->state == SDL_PRESSED);
			break;
		case SDLK_RETURN:
			set_bit(controllers[0].state, PAD_START, event->state == SDL_PRESSED);
			break;
		case SDLK_UP:
			set_bit(controllers[0].state, PAD_UP, event->state == SDL_PRESSED);
			break;
		case SDLK_DOWN:
			set_bit(controllers[0].state, PAD_DOWN, event->state == SDL_PRESSED);
			break;
		case SDLK_LEFT:
			set_bit(controllers[0].state, PAD_LEFT, event->state == SDL_PRESSED);
			break;
		case SDLK_RIGHT:
			set_bit(controllers[0].state, PAD_RIGHT, event->state == SDL_PRESSED);
			break;
		default:
			break;
	}
}


int main(int argc, char **argv) {
	if (argc != 2) {
		printf("Give me exactly one argument (the ROM file)");
		exit(-1);
	}

	FILE *fp = fopen(argv[1], "rb");
	if (fp == NULL) {
		printf("Failed loading ROM.\n");
		exit(-2);
	}

	rom_t *rom = read_rom(fp);
	print_rom_header(rom);
	printf("\n");

	mapper_t *mapper = mmc1_create_mapper(rom);
	free(rom);

	SDL_Init(SDL_INIT_EVERYTHING);

	controller_t controllers[2];
	init_controllers(mapper, &controllers[0]);

	ui_t ui = {
			.semaphore = SDL_CreateSemaphore(0)
	};

	SDL_AtomicSet(&ui.done, 0);
	SDL_Thread *ui_thread = SDL_CreateThread(&render_thread, "UI-thread", &ui);

	uint8_t cpu_skip = 1;

	Uint32 target = SDL_GetTicks();

	SDL_Event event;
	do {
		Uint32 time = SDL_GetTicks();
		if (time < target)
			SDL_Delay(target - time);
		else if (time > target) {
			printf("Missed frame! (delta: %u ms)\n", time - target);
			target = time;
		}

		target += 17;
		if (target % 50 == 1)
			--target;

		cpu_skip = emulate_frame(mapper, cpu_skip);

		memcpy(&ui.picture, &mapper->ppu.picture, 256 * 240);
		SDL_SemPost(ui.semaphore);

		while (SDL_PollEvent(&event) && event.type != SDL_QUIT) {
			switch (event.type) {
				case SDL_CONTROLLERDEVICEADDED:
					init_controllers(mapper, &controllers[0]);
					break;
				case SDL_CONTROLLERDEVICEREMOVED:
					destruct_controller(&controllers[0], event.cdevice.which);
					break;
				case SDL_CONTROLLERBUTTONDOWN:
				case SDL_CONTROLLERBUTTONUP:
					update_controller_state(&controllers[0], &event.cbutton);
					break;
				case SDL_CONTROLLERAXISMOTION:
					update_controller_axis(&controllers[0], &event.caxis);
					break;
				case SDL_KEYUP:
				case SDL_KEYDOWN:
					update_controller_state_kb(&controllers[0], &event.key);
					break;
				default:
					//printf("Unsupported event type %u\n", event.type);
					break;
			}
		}
	} while (event.type != SDL_QUIT);

	SDL_AtomicSet(&ui.done, 1);
	SDL_SemPost(ui.semaphore);
	SDL_WaitThread(ui_thread, &ui.result);

	SDL_Quit();

	printf("Frames: %lu\n", mapper->ppu.frame);

	free(mapper);
	return 0;
}

