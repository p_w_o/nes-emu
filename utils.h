#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>

void print_ubyte_as_binary(uint8_t data);

void print_ushort_as_binary(uint16_t data);

void dump_as_hex(uint8_t *data, size_t size, uint16_t address_offset);

#endif /* UTILS_H */

