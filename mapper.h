#ifndef MAPPER_H_
#define MAPPER_H_

#include <stdint.h>

#include "component.h"
#include "apu.h"
#include "cpu.h"
#include "dma.h"
#include "pad.h"
#include "ppu.h"


typedef struct {
	cpu_t cpu;
	ppu_t ppu;
	pad_t pad;
	apu_t apu;
	dma_t dma;
} mapper_t;


#endif /* MAPPER_H_ */
