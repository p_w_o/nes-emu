#ifndef COMPONENT_H
#define COMPONENT_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


typedef struct {
	uint16_t source_min_address;
    uint16_t source_max_address;
    uint16_t target_min_address;
    uint16_t target_length;
    uint8_t (*read_byte)(void *mapper, uint16_t address);
    void (*write_byte)(void *mapper, uint16_t address, uint8_t value);
} address_mapping_t;


typedef struct component {
	char *name;
	void *mapper;
	uint8_t mapping_count;
	address_mapping_t *address_mappings;
} component_t;



uint8_t read_ubyte(component_t component, uint16_t address);

uint16_t read_ushort(component_t component, uint16_t address);

void write_ubyte(component_t component, uint16_t address, uint8_t value);

void write_ushort(component_t component, uint16_t address, uint16_t value);

#endif /* COMPONENT_H */
