#include <stdio.h>

#include "pad.h"
#include "mapper.h"


void pad_init(pad_t *pad) {

}

int pad_emulate(pad_t *pad) {
	return 1;
}


uint8_t pad_read_ubyte(void *mapper, uint16_t address) {
	pad_t *pad = &((mapper_t*)mapper)->pad;

	uint8_t result;
	switch (address) {
		case 0x00:
			if (pad->reg_in) {
				pad->reg_out_0 = pad->pad_0_state;
				result = pad->reg_out_0 & 1;
			}
			else {
				result = pad->reg_out_0 & 1;
				pad->reg_out_0 >>= 1;
			}
			break;
		case 0x17:
			if (pad->reg_in) {
				pad->reg_out_1 = pad->pad_1_state;
				result = pad->reg_out_1 & 1;
			}
			else {
				result = pad->reg_out_1 & 1;
				pad->reg_out_1 >>= 1;
			}
			break;
		default:
			printf("Invalid read from pad address %04X\n", address);
			result = 0;
			break;
	}

	return result & 1;
}

void pad_write_ubyte(void *mapper, uint16_t address, uint8_t value) {
	if (address == 0) {
		pad_t *pad = &((mapper_t*)mapper)->pad;
		pad->reg_in = value & 1;
		pad->reg_out_0 = pad->pad_0_state;
		pad->reg_out_1 = pad->pad_1_state;
	}
	else {
		printf("Invalid write to pad address %04X, value $%02X\n", address, value);
	}
}
