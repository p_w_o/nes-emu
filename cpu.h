#ifndef CPU_H
#define CPU_H

#include <stdint.h>
#include "component.h"
#include "ines.h"


typedef struct {
	component_t super;

	uint8_t a;  	// accumulator
	uint8_t x;  	// index register x
	uint8_t y;  	// index register y
	uint16_t pc;	// program counter
	uint8_t s;  	// stack pointer
	uint8_t p;  	// state register

	uint8_t nmi;	// whether NMI was signaled
	uint64_t cycle; // cycles executed
} cpu_t;


void cpu_init(cpu_t *cpu);

uint8_t cpu_emulate(cpu_t *cpu);

#endif /* CPU_2A03_H */
