#include <cpu.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cpu.h"
#include "component.h"
#include "mapper.h"
#include "utils.h"


typedef enum {
	CARRY,
	ZERO,
	INTERRUPT,
	DECIMAL_MODE,
	BREAK,
	UNUSED,
	OVERFLOW,
	NEGATIVE
} flag_t;

typedef enum {
	ACCUMULATOR,
	IMPLIED,
	IMMEDIATE,
	RELATIVE,
	ZERO_PAGE,
	ZERO_PAGE_X,
	ZERO_PAGE_Y,
	INDIRECT,
	INDIRECT_X,
	INDIRECT_Y,
	INDIRECT_PLUS_Y,
	ABSOLUTE,
	ABSOLUTE_X,
	ABSOLUTE_Y
} addressing_t;


typedef struct {
	uint8_t opcode;
	char *name;
	addressing_t addressing;
	uint8_t cycles;
	uint8_t page_boundary_cycles;
} instruction_t;


const instruction_t instructions[256] = {
		{ .opcode = 0x00, .name = "BRK", .addressing = IMMEDIATE, .cycles = 7 },
		{ .opcode = 0x01, .name = "ORA", .addressing = INDIRECT_X, .cycles = 6 },
		{ .opcode = 0x02, .name = "???" },
		{ .opcode = 0x03, .name = "*SLO", .addressing = INDIRECT_X, .cycles = 8 },
		{ .opcode = 0x04, .name = "*IGN", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0x05, .name = "ORA", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0x06, .name = "ASL", .addressing = ZERO_PAGE, .cycles = 5 },
		{ .opcode = 0x07, .name = "*SLO", .addressing = ZERO_PAGE, .cycles = 5 },
		{ .opcode = 0x08, .name = "PHP", .addressing = IMPLIED, .cycles = 3 },
		{ .opcode = 0x09, .name = "ORA", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0x0A, .name = "ASL", .addressing = ACCUMULATOR, .cycles = 2 },
		{ .opcode = 0x0B, .name = "*ANC", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0x0C, .name = "*IGN", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0x0D, .name = "ORA", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0x0E, .name = "ASL", .addressing = ABSOLUTE, .cycles = 6 },
		{ .opcode = 0x03, .name = "*SLO", .addressing = ABSOLUTE, .cycles = 6 },
		{ .opcode = 0x10, .name = "BPL", .addressing = RELATIVE, .cycles = 2 },
		{ .opcode = 0x11, .name = "ORA", .addressing = INDIRECT_PLUS_Y, .cycles = 5, .page_boundary_cycles = 1 },
		{ .opcode = 0x12, .name = "???" },
		{ .opcode = 0x13, .name = "*SLO", .addressing = INDIRECT_PLUS_Y, .cycles = 8 },
		{ .opcode = 0x14, .name = "*IGN", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0x15, .name = "ORA", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0x16, .name = "ASL", .addressing = ZERO_PAGE_X, .cycles = 6 },
		{ .opcode = 0x03, .name = "*SLO", .addressing = ZERO_PAGE_X, .cycles = 6 },
		{ .opcode = 0x18, .name = "CLC", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0x19, .name = "ORA", .addressing = ABSOLUTE_Y, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0x1A, .name = "*NOP", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0x1B, .name = "*SLO", .addressing = ABSOLUTE_Y, .cycles = 7 },
		{ .opcode = 0x1C, .name = "*IGN", .addressing = ABSOLUTE_X, .cycles = 4 },
		{ .opcode = 0x1D, .name = "ORA", .addressing = ABSOLUTE_X, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0x1E, .name = "ASL", .addressing = ABSOLUTE_X, .cycles = 7 },
		{ .opcode = 0x1F, .name = "*SLO", .addressing = ABSOLUTE_X, .cycles = 7 },
		{ .opcode = 0x20, .name = "JSR", .addressing = ABSOLUTE, .cycles = 6 },
		{ .opcode = 0x21, .name = "AND", .addressing = INDIRECT_X, .cycles = 6 },
		{ .opcode = 0x22, .name = "???" },
		{ .opcode = 0x23, .name = "*RLA", .addressing = INDIRECT_X, .cycles = 8 },
		{ .opcode = 0x24, .name = "BIT", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0x25, .name = "AND", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0x26, .name = "ROL", .addressing = ZERO_PAGE, .cycles = 5 },
		{ .opcode = 0x27, .name = "*RLA", .addressing = ZERO_PAGE, .cycles = 5 },
		{ .opcode = 0x28, .name = "PLP", .addressing = IMPLIED, .cycles = 4 },
		{ .opcode = 0x29, .name = "AND", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0x2A, .name = "ROL", .addressing = ACCUMULATOR, .cycles = 2 },
		{ .opcode = 0x2B, .name = "*ANC", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0x2C, .name = "BIT", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0x2D, .name = "AND", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0x2E, .name = "ROL", .addressing = ABSOLUTE, .cycles = 6 },
		{ .opcode = 0x2F, .name = "*RLA", .addressing = ABSOLUTE, .cycles = 6 },
		{ .opcode = 0x30, .name = "BMI", .addressing = RELATIVE, .cycles = 2 },
		{ .opcode = 0x31, .name = "AND", .addressing = INDIRECT_PLUS_Y, .cycles = 5, .page_boundary_cycles = 1 },
		{ .opcode = 0x32, .name = "???" },
		{ .opcode = 0x33, .name = "*RLA", .addressing = INDIRECT_PLUS_Y, .cycles = 8 },
		{ .opcode = 0x34, .name = "*IGN", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0x35, .name = "AND", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0x36, .name = "ROL", .addressing = ZERO_PAGE_X, .cycles = 6 },
		{ .opcode = 0x37, .name = "*RLA", .addressing = ZERO_PAGE_X, .cycles = 6 },
		{ .opcode = 0x38, .name = "SEC", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0x39, .name = "AND", .addressing = ABSOLUTE_Y, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0x3A, .name = "*NOP", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0x3B, .name = "*RLA", .addressing = ABSOLUTE_Y, .cycles = 7 },
		{ .opcode = 0x3C, .name = "*IGN", .addressing = ABSOLUTE_X, .cycles = 4 },
		{ .opcode = 0x3D, .name = "AND", .addressing = ABSOLUTE_X, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0x3E, .name = "ROL", .addressing = ABSOLUTE_X, .cycles = 7 },
		{ .opcode = 0x3F, .name = "*RLA", .addressing = ABSOLUTE_X, .cycles = 7 },
		{ .opcode = 0x40, .name = "RTI", .addressing = IMPLIED, .cycles = 6 },
		{ .opcode = 0x41, .name = "EOR", .addressing = INDIRECT_X, .cycles = 6 },
		{ .opcode = 0x42, .name = "???" },
		{ .opcode = 0x43, .name = "*SRE", .addressing = INDIRECT_X, .cycles = 8 },
		{ .opcode = 0x44, .name = "*IGN", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0x45, .name = "EOR", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0x46, .name = "LSR", .addressing = ZERO_PAGE, .cycles = 5 },
		{ .opcode = 0x47, .name = "*SRE", .addressing = ZERO_PAGE, .cycles = 5 },
		{ .opcode = 0x48, .name = "PHA", .addressing = IMPLIED, .cycles = 3 },
		{ .opcode = 0x49, .name = "EOR", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0x4A, .name = "LSR", .addressing = ACCUMULATOR, .cycles = 2 },
		{ .opcode = 0x4B, .name = "*ALR", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0x4C, .name = "JMP", .addressing = ABSOLUTE, .cycles = 3 },
		{ .opcode = 0x4D, .name = "EOR", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0x4E, .name = "LSR", .addressing = ABSOLUTE, .cycles = 6 },
		{ .opcode = 0x4F, .name = "*SRE", .addressing = ABSOLUTE, .cycles = 6 },
		{ .opcode = 0x50, .name = "BVC", .addressing = RELATIVE, .cycles = 2 },
		{ .opcode = 0x51, .name = "EOR", .addressing = INDIRECT_PLUS_Y, .cycles = 5, .page_boundary_cycles = 1 },
		{ .opcode = 0x52, .name = "???" },
		{ .opcode = 0x53, .name = "*SRE", .addressing = INDIRECT_PLUS_Y, .cycles = 8 },
		{ .opcode = 0x54, .name = "*IGN", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0x55, .name = "EOR", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0x56, .name = "LSR", .addressing = ZERO_PAGE_X, .cycles = 6 },
		{ .opcode = 0x57, .name = "*SRE", .addressing = ZERO_PAGE_X, .cycles = 6 },
		{ .opcode = 0x58, .name = "CLI", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0x59, .name = "EOR", .addressing = ABSOLUTE_Y, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0x5A, .name = "*NOP", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0x5B, .name = "*SRE", .addressing = ABSOLUTE_Y, .cycles = 7 },
		{ .opcode = 0x5C, .name = "*IGN", .addressing = ABSOLUTE_X, .cycles = 4 },
		{ .opcode = 0x5D, .name = "EOR", .addressing = ABSOLUTE_X, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0x5E, .name = "LSR", .addressing = ABSOLUTE_X, .cycles = 7 },
		{ .opcode = 0x5B, .name = "*SRE", .addressing = ABSOLUTE_X, .cycles = 7 },
		{ .opcode = 0x60, .name = "RTS", .addressing = IMPLIED, .cycles = 6 },
		{ .opcode = 0x61, .name = "ADC", .addressing = INDIRECT_X, .cycles = 6 },
		{ .opcode = 0x62, .name = "???" },
		{ .opcode = 0x63, .name = "*RRA", .addressing = INDIRECT_X, .cycles = 8 },
		{ .opcode = 0x64, .name = "*IGN", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0x65, .name = "ADC", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0x66, .name = "ROR", .addressing = ZERO_PAGE, .cycles = 5 },
		{ .opcode = 0x67, .name = "*RRA", .addressing = ZERO_PAGE, .cycles = 5 },
		{ .opcode = 0x68, .name = "PLA", .addressing = IMPLIED, .cycles = 4 },
		{ .opcode = 0x69, .name = "ADC", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0x6A, .name = "ROR", .addressing = ACCUMULATOR, .cycles = 2 },
		{ .opcode = 0x6B, .name = "*ARR", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0x6C, .name = "JMP", .addressing = INDIRECT, .cycles = 5 },
		{ .opcode = 0x6D, .name = "ADC", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0x6E, .name = "ROR", .addressing = ABSOLUTE, .cycles = 6 },
		{ .opcode = 0x63, .name = "*RRA", .addressing = ABSOLUTE, .cycles = 6 },
		{ .opcode = 0x70, .name = "BVS", .addressing = RELATIVE, .cycles = 2 },
		{ .opcode = 0x71, .name = "ADC", .addressing = INDIRECT_PLUS_Y, .cycles = 5, .page_boundary_cycles = 1 },
		{ .opcode = 0x72, .name = "???" },
		{ .opcode = 0x73, .name = "*RRA", .addressing = INDIRECT_PLUS_Y, .cycles = 8 },
		{ .opcode = 0x74, .name = "*IGN", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0x75, .name = "ADC", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0x76, .name = "ROR", .addressing = ZERO_PAGE_X, .cycles = 6 },
		{ .opcode = 0x77, .name = "*RRA", .addressing = ZERO_PAGE_X, .cycles = 6 },
		{ .opcode = 0x78, .name = "SEI", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0x79, .name = "ADC", .addressing = ABSOLUTE_Y, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0x7A, .name = "*NOP", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0x7B, .name = "*RRA", .addressing = ABSOLUTE_Y, .cycles = 7 },
		{ .opcode = 0x7C, .name = "*IGN", .addressing = ABSOLUTE_X, .cycles = 4 },
		{ .opcode = 0x7D, .name = "ADC", .addressing = ABSOLUTE_X, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0x7E, .name = "ROR", .addressing = ABSOLUTE_X, .cycles = 7 },
		{ .opcode = 0x7F, .name = "*RRA", .addressing = ABSOLUTE_X, .cycles = 7 },
		{ .opcode = 0x80, .name = "*SKB", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0x81, .name = "STA", .addressing = INDIRECT_X, .cycles = 6 },
		{ .opcode = 0x82, .name = "*SKB", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0x83, .name = "*SAX", .addressing = INDIRECT_X, .cycles = 6 },
		{ .opcode = 0x84, .name = "STY", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0x85, .name = "STA", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0x86, .name = "STX", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0x87, .name = "*SAX", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0x88, .name = "DEY", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0x89, .name = "*SKB", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0x8A, .name = "TXA", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0x8B, .name = "*XAA", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0x8C, .name = "STY", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0x8D, .name = "STA", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0x8E, .name = "STX", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0x8F, .name = "*SAX", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0x90, .name = "BCC", .addressing = RELATIVE, .cycles = 2 },
		{ .opcode = 0x91, .name = "STA", .addressing = INDIRECT_PLUS_Y, .cycles = 6 },
		{ .opcode = 0x92, .name = "???" },
		{ .opcode = 0x93, .name = "???" },
		{ .opcode = 0x94, .name = "STY", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0x95, .name = "STA", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0x96, .name = "STX", .addressing = ZERO_PAGE_Y, .cycles = 4 },
		{ .opcode = 0x97, .name = "*SAX", .addressing = ZERO_PAGE_Y, .cycles = 4 },
		{ .opcode = 0x98, .name = "TYA", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0x99, .name = "STA", .addressing = ABSOLUTE_Y, .cycles = 5 },
		{ .opcode = 0x9A, .name = "TXS", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0x9B, .name = "???" },
		{ .opcode = 0x9C, .name = "*SHY", .addressing = ABSOLUTE_X, .cycles = 5 },
		{ .opcode = 0x9D, .name = "STA", .addressing = ABSOLUTE_X, .cycles = 5 },
		{ .opcode = 0x9E, .name = "*SHX", .addressing = ABSOLUTE_Y, .cycles = 5 },
		{ .opcode = 0x9F, .name = "???" },
		{ .opcode = 0xA0, .name = "LDY", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0xA1, .name = "LDA", .addressing = INDIRECT_X, .cycles = 6 },
		{ .opcode = 0xA2, .name = "LDX", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0xA3, .name = "*LAX", .addressing = INDIRECT_X, .cycles = 6 },
		{ .opcode = 0xA4, .name = "LDY", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0xA5, .name = "LDA", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0xA6, .name = "LDX", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0xA7, .name = "*LAX", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0xA8, .name = "TAY", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0xA9, .name = "LDA", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0xAA, .name = "TAX", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0xAB, .name = "*LAX", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0xAC, .name = "LDY", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0xAD, .name = "LDA", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0xAE, .name = "LDX", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0xAF, .name = "*LAX", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0xB0, .name = "BCS", .addressing = RELATIVE, .cycles = 2 },
		{ .opcode = 0xB1, .name = "LDA", .addressing = INDIRECT_PLUS_Y, .cycles = 5, .page_boundary_cycles = 1 },
		{ .opcode = 0xB2, .name = "???" },
		{ .opcode = 0xB3, .name = "*LAX", .addressing = INDIRECT_PLUS_Y, .cycles = 5 },
		{ .opcode = 0xB4, .name = "LDY", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0xB5, .name = "LDA", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0xB6, .name = "LDX", .addressing = ZERO_PAGE_Y, .cycles = 4 },
		{ .opcode = 0xB7, .name = "*LAX", .addressing = ZERO_PAGE_Y, .cycles = 4 },
		{ .opcode = 0xB8, .name = "CLV", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0xB9, .name = "LDA", .addressing = ABSOLUTE_Y, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0xBA, .name = "TSX", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0xBB, .name = "???" },
		{ .opcode = 0xBC, .name = "LDY", .addressing = ABSOLUTE_X, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0xBD, .name = "LDA", .addressing = ABSOLUTE_X, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0xBE, .name = "LDX", .addressing = ABSOLUTE_Y, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0xBF, .name = "*LAX", .addressing = ABSOLUTE_Y, .cycles = 4 },
		{ .opcode = 0xC0, .name = "CPY", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0xC1, .name = "CMP", .addressing = INDIRECT_X, .cycles = 6 },
		{ .opcode = 0xC2, .name = "*SKB", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0xC3, .name = "*DCP", .addressing = INDIRECT_X, .cycles = 8 },
		{ .opcode = 0xC4, .name = "CPY", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0xC5, .name = "CMP", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0xC6, .name = "DEC", .addressing = ZERO_PAGE, .cycles = 5 },
		{ .opcode = 0xC7, .name = "*DCP", .addressing = ZERO_PAGE, .cycles = 5 },
		{ .opcode = 0xC8, .name = "INY", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0xC9, .name = "CMP", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0xCA, .name = "DEX", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0xCB, .name = "*AXS", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0xCC, .name = "CPY", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0xCD, .name = "CMP", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0xCE, .name = "DEC", .addressing = ABSOLUTE, .cycles = 6 },
		{ .opcode = 0xCF, .name = "*DCP", .addressing = ABSOLUTE, .cycles = 6 },
		{ .opcode = 0xD0, .name = "BNE", .addressing = RELATIVE, .cycles = 2 },
		{ .opcode = 0xD1, .name = "CMP", .addressing = INDIRECT_PLUS_Y, .cycles = 5, .page_boundary_cycles = 1 },
		{ .opcode = 0xD2, .name = "???" },
		{ .opcode = 0xD3, .name = "*DCP", .addressing = INDIRECT_PLUS_Y, .cycles = 8 },
		{ .opcode = 0xD4, .name = "*IGN", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0xD5, .name = "CMP", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0xD6, .name = "DEC", .addressing = ZERO_PAGE_X, .cycles = 6 },
		{ .opcode = 0xD7, .name = "*DCP", .addressing = ZERO_PAGE_X, .cycles = 6 },
		{ .opcode = 0xD8, .name = "CLD", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0xD9, .name = "CMP", .addressing = ABSOLUTE_Y, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0xDA, .name = "*NOP", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0xDB, .name = "*DCP", .addressing = ABSOLUTE_Y, .cycles = 7 },
		{ .opcode = 0xDC, .name = "*IGN", .addressing = ABSOLUTE_X, .cycles = 4 },
		{ .opcode = 0xDD, .name = "CMP", .addressing = ABSOLUTE_X, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0xDE, .name = "DEC", .addressing = ABSOLUTE_X, .cycles = 7 },
		{ .opcode = 0xDF, .name = "*DCP", .addressing = ABSOLUTE_X, .cycles = 7 },
		{ .opcode = 0xE0, .name = "CPX", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0xE1, .name = "SBC", .addressing = INDIRECT_X, .cycles = 6 },
		{ .opcode = 0xE2, .name = "*SKB", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0xE3, .name = "*ISC", .addressing = INDIRECT_X, .cycles = 8 },
		{ .opcode = 0xE4, .name = "CPX", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0xE5, .name = "SBC", .addressing = ZERO_PAGE, .cycles = 3 },
		{ .opcode = 0xE6, .name = "INC", .addressing = ZERO_PAGE, .cycles = 5 },
		{ .opcode = 0xE7, .name = "*ISC", .addressing = ZERO_PAGE, .cycles = 8 },
		{ .opcode = 0xE8, .name = "INX", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0xE9, .name = "SBC", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0xEA, .name = "NOP", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0xEB, .name = "*SBC", .addressing = IMMEDIATE, .cycles = 2 },
		{ .opcode = 0xEC, .name = "CPX", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0xED, .name = "SBC", .addressing = ABSOLUTE, .cycles = 4 },
		{ .opcode = 0xEE, .name = "INC", .addressing = ABSOLUTE, .cycles = 6 },
		{ .opcode = 0xEF, .name = "*ISC", .addressing = ABSOLUTE, .cycles = 5 },
		{ .opcode = 0xF0, .name = "BEQ", .addressing = RELATIVE, .cycles = 2 },
		{ .opcode = 0xF1, .name = "SBC", .addressing = INDIRECT_PLUS_Y, .cycles = 5, .page_boundary_cycles = 1 },
		{ .opcode = 0xF2, .name = "???" },
		{ .opcode = 0xF3, .name = "*ISC", .addressing = INDIRECT_PLUS_Y, .cycles = 8 },
		{ .opcode = 0xF4, .name = "*IGN", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0xF5, .name = "SBC", .addressing = ZERO_PAGE_X, .cycles = 4 },
		{ .opcode = 0xF6, .name = "INC", .addressing = ZERO_PAGE_X, .cycles = 6 },
		{ .opcode = 0xF7, .name = "*ISC", .addressing = ZERO_PAGE_X, .cycles = 6 },
		{ .opcode = 0xF8, .name = "SED", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0xF9, .name = "SBC", .addressing = ABSOLUTE_Y, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0xFA, .name = "*NOP", .addressing = IMPLIED, .cycles = 2 },
		{ .opcode = 0xFB, .name = "*ISC", .addressing = ABSOLUTE_Y, .cycles = 7 },
		{ .opcode = 0xFC, .name = "*IGN", .addressing = ABSOLUTE_X, .cycles = 4 },
		{ .opcode = 0xFD, .name = "SBC", .addressing = ABSOLUTE_X, .cycles = 4, .page_boundary_cycles = 1 },
		{ .opcode = 0xFE, .name = "INC", .addressing = ABSOLUTE_X, .cycles = 7 },
		{ .opcode = 0xFF, .name = "*ISC", .addressing = ABSOLUTE_X, .cycles = 7 }
};



static void push_ubyte(cpu_t *cpu, uint8_t value) {
	//printf("PUSH 0x%02X\n", value);
	write_ubyte(cpu->super, 0x0100 + cpu->s--, value);
}

static void push_ushort(cpu_t *cpu, uint16_t value) {
	push_ubyte(cpu, value >> 8);
	push_ubyte(cpu, value);
}

static uint8_t pop_ubyte(cpu_t *cpu) {
	//printf("POP 0x%02X\n", read_ubyte(cpu->super, 0x0100 + cpu->s + 1));
	return read_ubyte(cpu->super, 0x0100 + ++cpu->s);
}

static uint16_t pop_ushort(cpu_t *cpu) {
	return pop_ubyte(cpu) + ((uint16_t)pop_ubyte(cpu) << 8);
}


static int8_t ubyte_to_byte(uint8_t value) {
	return (int8_t)value;
}


static uint8_t read_ubyte_operand(cpu_t *cpu) {
	return read_ubyte(cpu->super, cpu->pc + 1);
}

static uint16_t read_ushort_operand(cpu_t *cpu) {
	return read_ushort(cpu->super, cpu->pc + 1);
}


static uint16_t read_zp_indirection(component_t component, uint8_t address) {
	return read_ubyte(component, address) + read_ubyte(component, (address + 1) & 0xFF) * 256;
}

static uint16_t read_jmp_indirection(component_t component, uint16_t address) {
	uint16_t second_address = (address & 0xFF00) | ((address + 1) & 0xFF);
	return read_ubyte(component, address) + read_ubyte(component, second_address) * 256;
}


static uint16_t add_page_boundary_cost(cpu_t *cpu, const instruction_t *instruction, uint16_t base, uint16_t index) {
	const uint16_t result = base + index;
	if ((result & 0xFF00) != (base & 0xFF00))
		cpu->cycle += instruction->page_boundary_cycles;
	return result;
}


static uint16_t get_address(cpu_t *cpu, const instruction_t *instruction) {
	switch (instruction->addressing) {
		case RELATIVE:       		return ubyte_to_byte(read_ubyte_operand(cpu)) + cpu->pc;
		case ZERO_PAGE:      		return read_ubyte_operand(cpu);
		case ZERO_PAGE_X:    		return (read_ubyte_operand(cpu) + cpu->x) & 0xFF;
		case ZERO_PAGE_Y:    		return (read_ubyte_operand(cpu) + cpu->y) & 0xFF;
		case INDIRECT:       		return read_jmp_indirection(cpu->super, read_ushort_operand(cpu));
		case INDIRECT_X:     		return read_zp_indirection(cpu->super, read_ubyte_operand(cpu) + cpu->x);
		case INDIRECT_Y:     		return read_zp_indirection(cpu->super, read_ubyte_operand(cpu) + cpu->y);
		case INDIRECT_PLUS_Y:		return add_page_boundary_cost(cpu, instruction, read_zp_indirection(cpu->super, read_ubyte_operand(cpu)), cpu->y);
		case ABSOLUTE:       		return read_ushort_operand(cpu);
		case ABSOLUTE_X:     		return add_page_boundary_cost(cpu, instruction, read_ushort_operand(cpu), cpu->x);
		case ABSOLUTE_Y:     		return add_page_boundary_cost(cpu, instruction, read_ushort_operand(cpu), cpu->y);
		default:
			printf("Unknown addressing type %u\n", instruction->addressing);
			exit(-3);
	}
}


static uint16_t get_operand(cpu_t *cpu, const instruction_t *instruction) {
	switch (instruction->addressing) {
		case ACCUMULATOR:         	return cpu->a;
		case IMPLIED:             	return 0;
		case IMMEDIATE:            	return read_ubyte_operand(cpu);
		default:
			return read_ubyte(cpu->super, get_address(cpu, instruction));
	}
}


static uint8_t get_size(const instruction_t *instruction) {
	switch (instruction->addressing) {
		case ABSOLUTE:       		return 3;
		case ABSOLUTE_X:     		return 3;
		case ABSOLUTE_Y:     		return 3;
		case ACCUMULATOR:    		return 1;
		case IMMEDIATE:      		return 2;
		case IMPLIED:        		return 1;
		case INDIRECT_PLUS_Y:		return 2;
		case INDIRECT:       		return 3;
		case INDIRECT_X:     		return 2;
		case INDIRECT_Y:     		return 2;
		case RELATIVE:       		return 2;
		case ZERO_PAGE:      		return 2;
		case ZERO_PAGE_X:    		return 2;
		case ZERO_PAGE_Y:    		return 2;
		default:
			printf("Unknown addressing type");
			exit(-10);
	}
}


void cpu_init(cpu_t *cpu) {
	cpu->super.name = "CPU";
	cpu->a = 0;
	cpu->x = 0;
	cpu->y = 0;
	cpu->pc = read_ushort(cpu->super, 0xFFFC);
	cpu->s = 0xFD;
	cpu->p = (1 << INTERRUPT) | (1 << UNUSED);
	cpu->cycle = 0;
}


static void set_flag(cpu_t *cpu, flag_t flag, uint16_t value) {
	cpu->p ^= (-!!value ^ cpu->p) & (1 << flag);
}

static uint8_t get_flag(cpu_t *cpu, flag_t flag) {
	return (cpu->p >> flag) & 1;
}


static void print_operand(cpu_t *cpu) {
	const instruction_t *instruction = &instructions[read_ubyte(cpu->super, cpu->pc)];

	int length;
	switch (instruction->addressing) {
		case ACCUMULATOR:
			printf("A");
			length = 1;
			break;
		case IMPLIED:
			length = 0;
			break;
		case IMMEDIATE:
			printf("#$%02X", read_ubyte(cpu->super, cpu->pc + 1));
			length = 4;
			break;
		case RELATIVE:
			printf("$%04X", cpu->pc + ubyte_to_byte(read_ubyte(cpu->super, cpu->pc + 1)) + get_size(instruction));
			length = 5;
			break;
		case ZERO_PAGE:
			printf("$%02X", read_ubyte(cpu->super, cpu->pc + 1));
			length = 3;
			break;
		case ZERO_PAGE_X:
			printf("$%02X,X", read_ubyte(cpu->super, cpu->pc + 1));
			length = 5;
			break;
		case ZERO_PAGE_Y:
			printf("$%02X,Y", read_ubyte(cpu->super, cpu->pc + 1));
			length = 5;
			break;
		case INDIRECT:
			printf("($%04X)", read_ushort(cpu->super, cpu->pc + 1));
			length = 7;
			break;
		case INDIRECT_X:
			printf("($%02X,X)", read_ubyte(cpu->super, cpu->pc + 1));
			length = 7;
			break;
		case INDIRECT_Y:
			printf("($%02X,Y)", read_ubyte(cpu->super, cpu->pc + 1));
			length = 7;
			break;
		case INDIRECT_PLUS_Y:
			printf("($%02X),Y", read_ubyte(cpu->super, cpu->pc + 1));
			length = 7;
			break;
		case ABSOLUTE:
			printf("$%04X", read_ushort(cpu->super, cpu->pc + 1));
			length = 5;
			break;
		case ABSOLUTE_X:
			printf("$%04X,X", read_ushort(cpu->super, cpu->pc + 1));
			length = 7;
			break;
		case ABSOLUTE_Y:
			printf("$%04X,Y", read_ushort(cpu->super, cpu->pc + 1));
			length = 7;
			break;
		default:
			printf("Unknown addressing type %d\n", instruction->addressing);
			exit(-3);
	}

	for (int i = length; i < 28; ++i)
		printf(" ");
}


static void print_state(cpu_t *cpu) {
	const instruction_t *instruction = &instructions[read_ubyte(cpu->super, cpu->pc)];

	printf("%04X  ", cpu->pc);
	for (int i = 0; i < 3; ++i) {
		if (i < get_size(instruction))
			printf("%02X ", read_ubyte(cpu->super, cpu->pc + i));
		else
			printf("   ");
	}

	printf("%4s ", instruction->name);
	print_operand(cpu);

	printf("A:%02X X:%02X Y:%02X P:%02X SP:%02X CYC:%3u SL:%u\n",
			cpu->a,
			cpu->x,
			cpu->y,
			cpu->p,
			cpu->s,
			((mapper_t*)cpu->super.mapper)->ppu.scan_line_cycle,
			((mapper_t*)cpu->super.mapper)->ppu.scan_line);
}


static uint8_t update_cmp_flags(cpu_t *cpu, uint8_t value) {
	set_flag(cpu, ZERO, value == 0);
	set_flag(cpu, NEGATIVE, value >= 128);
	return value;
}


static void relative_jump(cpu_t *cpu, const instruction_t *instruction) {
	cpu->cycle++;
	uint16_t target = get_address(cpu, instruction);
	if ((target & 0xFF00) != (cpu->pc & 0xFF00))
		cpu->cycle++;
	cpu->pc = target;
}


uint8_t cpu_emulate(cpu_t *cpu) {
	if (cpu->nmi) {
		printf("!!! NMI !!!\n");
		push_ushort(cpu, cpu->pc);
		push_ubyte(cpu, cpu->p);
		cpu->pc = read_ushort(cpu->super, 0xFFFA);
		cpu->nmi = 0;
	}

	uint64_t start_cycle = cpu->cycle;

	print_state(cpu);
	uint8_t opcode = read_ubyte(cpu->super, cpu->pc);
	const instruction_t *instruction = &instructions[opcode];

	switch (opcode) {
		case 0x00:	// BRK
			push_ushort(cpu, cpu->pc + get_size(instruction));
			push_ubyte(cpu, cpu->p | 0x30);
			set_flag(cpu, INTERRUPT, 1);
			cpu->pc = read_ushort(cpu->super, 0xFFFE) - get_size(instruction);
			break;
		case 0x01:	// ORA
		case 0x05:
		case 0x09:
		case 0x0D:
		case 0x11:
		case 0x15:
		case 0x19:
		case 0x1D:
			cpu->a = update_cmp_flags(cpu, cpu->a | get_operand(cpu, instruction));
			break;
		case 0x03:	// *SLO
		case 0x07:
		case 0x0F:
		case 0x13:
		case 0x17:
		case 0x1B:
		case 0x1F: {
			uint16_t address = get_address(cpu, instruction);
			uint8_t operand = read_ubyte(cpu->super, address);
			uint8_t result = operand << 1;
			write_ubyte(cpu->super, address, result);
			set_flag(cpu, CARRY, operand & 0x80);
			cpu->a = update_cmp_flags(cpu, cpu->a | result);
			break;
		}
		case 0x06:	// ASL (mem)
		case 0x0E:
		case 0x16:
		case 0x1E: {
			uint16_t address = get_address(cpu, instruction);
			uint8_t operand = read_ubyte(cpu->super, address);
			write_ubyte(cpu->super, address, update_cmp_flags(cpu, operand << 1));
			set_flag(cpu, CARRY, operand & 0x80);
			break;
		}
		case 0x08:	// PHP
			push_ubyte(cpu, cpu->p | 0x10);
			break;
		case 0x0A: {// ASL (A)
			set_flag(cpu, CARRY, cpu->a & 0x80);
			update_cmp_flags(cpu, cpu->a <<= 1);
			break;
		}
		case 0x0B:	// *ANC
		case 0x2B:
			cpu->a = update_cmp_flags(cpu, cpu->a & get_operand(cpu, instruction));
			set_flag(cpu, CARRY, get_flag(cpu, NEGATIVE));
			break;
		case 0x04:	// *IGN
		case 0x0C:
		case 0x14:
		case 0x1C:
		case 0x34:
		case 0x3C:
		case 0x44:
		case 0x54:
		case 0x5C:
		case 0x64:
		case 0x74:
		case 0x7C:
		case 0xD4:
		case 0xDC:
		case 0xF4:
		case 0xFC:
			get_operand(cpu, instruction);
			break;
		case 0x10:	// BPL
			if (!get_flag(cpu, NEGATIVE))
				relative_jump(cpu, instruction);
			break;
		case 0x18:	// CLC
			set_flag(cpu, CARRY, 0);
			break;
		case 0x20:	// JSR
			push_ushort(cpu, cpu->pc + get_size(instruction) - 1);
			cpu->pc = get_address(cpu, instruction) - get_size(instruction);
			break;
		case 0x21:	// AND
		case 0x25:
		case 0x29:
		case 0x2D:
		case 0x31:
		case 0x35:
		case 0x39:
		case 0x3D:
			cpu->a = update_cmp_flags(cpu, cpu->a & get_operand(cpu, instruction));
			break;
		case 0x23:	// *RLA
		case 0x27:
		case 0x2F:
		case 0x33:
		case 0x37:
		case 0x3B:
		case 0x3F:  {
			uint16_t address = get_address(cpu, instruction);
			uint8_t operand = read_ubyte(cpu->super, address);
			uint8_t result = (operand << 1) | get_flag(cpu, CARRY);
			write_ubyte(cpu->super, address, result);
			set_flag(cpu, CARRY, operand & 0x80);
			cpu->a = update_cmp_flags(cpu, cpu->a & result);
			break;
		}
		case 0x24:	// BIT
		case 0x2C: {
			uint8_t operand = get_operand(cpu, instruction);
			set_flag(cpu, NEGATIVE, operand & 0x80);
			set_flag(cpu, OVERFLOW, operand & 0x40);
			set_flag(cpu, ZERO, (operand & cpu->a) == 0);
			break;
		}
		case 0x26:	// ROL (mem)
		case 0x2E:
		case 0x36:
		case 0x3E: {
			uint16_t address = get_address(cpu, instruction);
			uint8_t operand = read_ubyte(cpu->super, address);
			write_ubyte(cpu->super, address, update_cmp_flags(cpu, (operand << 1) | get_flag(cpu, CARRY)));
			set_flag(cpu, CARRY, operand & 0x80);
			break;
		}
		case 0x28:	// PLP
			cpu->p = pop_ubyte(cpu);
			set_flag(cpu, BREAK, 0);
			set_flag(cpu, UNUSED, 1);
			break;
		case 0x2A: {// ROL (A)
			uint8_t operand = cpu->a;
			cpu->a = update_cmp_flags(cpu, (operand << 1) | get_flag(cpu, CARRY));
			set_flag(cpu, CARRY, operand & 0x80);
			break;
		}
		case 0x30:	// BMI
			if (get_flag(cpu, NEGATIVE))
				relative_jump(cpu, instruction);
			break;
		case 0x38:	// SEC
			set_flag(cpu, CARRY, 1);
			break;
		case 0x40:	// RTI
			cpu->p = pop_ubyte(cpu);
			set_flag(cpu, BREAK, 0);
			set_flag(cpu, UNUSED, 1);
			cpu->pc = pop_ushort(cpu) - get_size(instruction);
			break;
		case 0x41:	// EOR
		case 0x45:
		case 0x49:
		case 0x4D:
		case 0x51:
		case 0x55:
		case 0x59:
		case 0x5D:
			cpu->a = update_cmp_flags(cpu, get_operand(cpu, instruction) ^ cpu->a);
			break;
		case 0x43:
		case 0x47:
		case 0x4F:
		case 0x53:
		case 0x57:
		case 0x5B:
		case 0x5F: {
			uint16_t address = get_address(cpu, instruction);
			uint8_t operand = read_ubyte(cpu->super, address);
			uint8_t result = operand >> 1;
			write_ubyte(cpu->super, address, result);
			set_flag(cpu, CARRY, operand & 1);
			cpu->a = update_cmp_flags(cpu, result ^ cpu->a);
			break;
		}
		case 0x46:	// LSR (mem)
		case 0x4E:
		case 0x56:
		case 0x5E: {
			uint16_t address = get_address(cpu, instruction);
			uint8_t operand = read_ubyte(cpu->super, address);
			write_ubyte(cpu->super, address, update_cmp_flags(cpu, operand >> 1));
			set_flag(cpu, CARRY, operand & 1);
			break;
		}
		case 0x48:	// PHA
			push_ubyte(cpu, cpu->a);
			break;
		case 0x4A: {// LSR (A)
			set_flag(cpu, CARRY, cpu->a & 1);
			update_cmp_flags(cpu, cpu->a >>= 1);
			break;
		}
		case 0x4B:	// *ALR
			cpu->a = update_cmp_flags(cpu, cpu->a & get_operand(cpu, instruction));
			set_flag(cpu, CARRY, cpu->a & 1);
			update_cmp_flags(cpu, cpu->a >>= 1);
			break;
		case 0x4C:	// JMP
		case 0x6C:
			cpu->pc = get_address(cpu, instruction) - get_size(instruction);
			break;
		case 0x50:	// BVC
			if (!get_flag(cpu, OVERFLOW))
				relative_jump(cpu, instruction);
			break;
		case 0x58:	// CLI
			set_flag(cpu, INTERRUPT, 0);
			break;
		case 0x60:	// RTS
			cpu->pc = pop_ushort(cpu) + 1 - get_size(instruction);
			break;
		case 0x61:	// ADC
		case 0x65:
		case 0x69:
		case 0x6D:
		case 0x71:
		case 0x75:
		case 0x79:
		case 0x7D: {
			uint8_t operand = get_operand(cpu, instruction);
			uint16_t result = cpu->a + operand + get_flag(cpu, CARRY);
			update_cmp_flags(cpu, result);
			set_flag(cpu, CARRY, result & 0x100);
			set_flag(cpu, OVERFLOW, !((cpu->a ^ operand) & 0x80) && ((cpu->a ^ result) & 0x80));
			cpu->a = result;
			break;
		}
		case 0x63:	// *RRA
		case 0x67:
		case 0x6F:
		case 0x73:
		case 0x77:
		case 0x7B:
		case 0x7F: {
			uint16_t address = get_address(cpu, instruction);
			uint8_t operand = read_ubyte(cpu->super, address);
			uint8_t operand2 = (operand >> 1) | (get_flag(cpu, CARRY) << 7);
			write_ubyte(cpu->super, address, operand2);

			uint16_t result = cpu->a + operand2 + (operand & 1);
			update_cmp_flags(cpu, result);
			set_flag(cpu, CARRY, result & 0x100);
			set_flag(cpu, OVERFLOW, !((cpu->a ^ operand2) & 0x80) && ((cpu->a ^ result) & 0x80));
			cpu->a = result;
			break;
		}
		case 0x68:	// PLA
			cpu->a = update_cmp_flags(cpu, pop_ubyte(cpu));
			break;
		case 0x66:	// ROR (mem)
		case 0x6E:
		case 0x76:
		case 0x7E: {
			uint16_t address = get_address(cpu, instruction);
			uint8_t operand = read_ubyte(cpu->super, address);
			write_ubyte(cpu->super, address, update_cmp_flags(cpu, (operand >> 1) | (get_flag(cpu, CARRY) << 7)));
			set_flag(cpu, CARRY, operand & 1);
			break;
		}
		case 0x6A: {// ROR (A)
			uint8_t operand = cpu->a;
			cpu->a = update_cmp_flags(cpu, (operand >> 1) | (get_flag(cpu, CARRY) << 7));
			set_flag(cpu, CARRY, operand & 1);
			break;
		}
		case 0x6B: {// *ARR
			cpu->a = update_cmp_flags(cpu, cpu->a & get_operand(cpu, instruction));
			cpu->a = update_cmp_flags(cpu, (cpu->a >> 1) | (get_flag(cpu, CARRY) << 7));
			set_flag(cpu, CARRY, cpu->a & 0x40);
			set_flag(cpu, OVERFLOW, !(cpu->a & 0x40) != !(cpu->a & 0x20));
			break;
		}
		case 0x70:	// BVS
			if (get_flag(cpu, OVERFLOW))
				relative_jump(cpu, instruction);
			break;
		case 0x78:	// SEI
			set_flag(cpu, INTERRUPT, 1);
			break;
		case 0x81:	// STA
		case 0x85:
		case 0x8D:
		case 0x91:
		case 0x95:
		case 0x99:
		case 0x9D:
			write_ubyte(cpu->super, get_address(cpu, instruction), cpu->a);
			break;
		case 0x83:	//*SAX
		case 0x87:
		case 0x8F:
		case 0x97:
			write_ubyte(cpu->super, get_address(cpu, instruction), cpu->a & cpu->x);
			break;
		case 0x84:	// STY
		case 0x8C:
		case 0x94:
			write_ubyte(cpu->super, get_address(cpu, instruction), cpu->y);
			break;
		case 0x8A:	// TXA
			update_cmp_flags(cpu, cpu->a = cpu->x);
			break;
		case 0x8B:	// *XAA
			cpu->a = update_cmp_flags(cpu, cpu->a & cpu->x & get_operand(cpu, instruction));
			break;
		case 0x86:	// STX
		case 0x8E:
		case 0x96:
			write_ubyte(cpu->super, get_address(cpu, instruction), cpu->x);
			break;
		case 0x88:	// DEY
			update_cmp_flags(cpu, --cpu->y);
			break;
		case 0x90:	// BCC
			if (!get_flag(cpu, CARRY))
				relative_jump(cpu, instruction);
			break;
		case 0x98:	// TYA
			cpu->a = update_cmp_flags(cpu, cpu->y);
			break;
		case 0x9A:	// TXS
			cpu->s = cpu->x;
			break;
		case 0x9C: {// *SHY
			uint16_t address = get_address(cpu, instruction);
			uint8_t result = (read_ubyte(cpu->super, cpu->pc + 2) + 1) & cpu->y;
			if ((address & 0xFF) < cpu->x)
				address = (result << 8) | (address & 0xFF);
			write_ubyte(cpu->super, address, result);
			break;
		}
		case 0x9E: {// *SHX
			uint16_t address = get_address(cpu, instruction);
			uint8_t result = (read_ubyte(cpu->super, cpu->pc + 2) + 1) & cpu->x;
			if ((address & 0xFF) < cpu->y)
				address = (result << 8) | (address & 0xFF);
			write_ubyte(cpu->super, address, result);
			break;
		}
		case 0xA0:	// LDY
		case 0xA4:
		case 0xAC:
		case 0xB4:
		case 0xBC:
			cpu->y = update_cmp_flags(cpu, get_operand(cpu, instruction));
			break;
		case 0xA1:	// LDA
		case 0xA5:
		case 0xA9:
		case 0xAD:
		case 0xB1:
		case 0xB5:
		case 0xB9:
		case 0xBD:
			cpu->a = update_cmp_flags(cpu, get_operand(cpu, instruction));
			break;
		case 0xA2:	// LDX
		case 0xA6:
		case 0xAE:
		case 0xB6:
		case 0xBE:
			cpu->x = update_cmp_flags(cpu, get_operand(cpu, instruction));
			break;
		case 0xA3:	// *LAX
		case 0xA7:
		case 0xAB:
		case 0xAF:
		case 0xB3:
		case 0xB7:
		case 0xBF:
			cpu->x = cpu->a = update_cmp_flags(cpu, get_operand(cpu, instruction));
			break;
		case 0xA8:	// TAY
			cpu->y = update_cmp_flags(cpu, cpu->a);
			break;
		case 0xAA:	// TAX
			cpu->x = update_cmp_flags(cpu, cpu->a);
			break;
		case 0xB0:	// BCS
			if (get_flag(cpu, CARRY))
				relative_jump(cpu, instruction);
			break;
		case 0xB8:	// CLV
			set_flag(cpu, OVERFLOW, 0);
			break;
		case 0xBA:	// TSX
			cpu->x = update_cmp_flags(cpu, cpu->s);
			break;
		case 0xC0:	// CPY
		case 0xC4:
		case 0xCC: {
			uint8_t operand = get_operand(cpu, instruction);
			update_cmp_flags(cpu, cpu->y - operand);
			set_flag(cpu, CARRY, operand <= cpu->y);
			break;
		}
		case 0xC1:	// CMP
		case 0xC5:
		case 0xC9:
		case 0xCD:
		case 0xD1:
		case 0xD5:
		case 0xD9:
		case 0xDD: {
			uint8_t operand = get_operand(cpu, instruction);
			update_cmp_flags(cpu, cpu->a - operand);
			set_flag(cpu, CARRY, operand <= cpu->a);
			break;
		}
		case 0xC3:	// *DCP
		case 0xC7:
		case 0xCF:
		case 0xD3:
		case 0xD7:
		case 0xDB:
		case 0xDF: {
			uint16_t address = get_address(cpu, instruction);
			uint8_t value = read_ubyte(cpu->super, address) - 1;
			write_ubyte(cpu->super, address, value);
			update_cmp_flags(cpu, cpu->a - value);
			set_flag(cpu, CARRY, value <= cpu->a);
			break;
		}
		case 0xC6:	// DEC
		case 0xCE:
		case 0xD6:
		case 0xDE: {
			uint16_t address = get_address(cpu, instruction);
			uint8_t value = read_ubyte(cpu->super, address) - 1;
			update_cmp_flags(cpu, value);
			write_ubyte(cpu->super, address, value);
			break;
		}
		case 0xC8:	// INY
			update_cmp_flags(cpu, ++cpu->y);
			break;
		case 0xCA:	// DEX
			update_cmp_flags(cpu, --cpu->x);
			break;
		case 0xCB: {// *AXS
			uint16_t result = (cpu->x & cpu->a) - get_operand(cpu, instruction);
			set_flag(cpu, CARRY, result < 0x100);
			cpu->x = result;
			update_cmp_flags(cpu, cpu->x);
			break;
		}
		case 0xD0:	// BNE
			if (!get_flag(cpu, ZERO))
				relative_jump(cpu, instruction);
			break;
		case 0xD8:	// CLD
			set_flag(cpu, DECIMAL_MODE, 0);
			break;
		case 0xE0:	// CPX
		case 0xE4:
		case 0xEC:  {
			uint8_t operand = get_operand(cpu, instruction);
			update_cmp_flags(cpu, cpu->x - operand);
			set_flag(cpu, CARRY, operand <= cpu->x);
			break;
		}
		case 0xE3:	// *ISC
		case 0xE7:
		case 0xEF:
		case 0xF3:
		case 0xF7:
		case 0xFB:
		case 0xFF: {
			uint16_t address = get_address(cpu, instruction);
			uint8_t value = read_ubyte(cpu->super, address) + 1;
			write_ubyte(cpu->super, address, value);
			uint16_t result = cpu->a - value - !get_flag(cpu, CARRY);
			set_flag(cpu, CARRY, result <= 0xFF);
			set_flag(cpu, OVERFLOW, ((cpu->a ^ value) & 0x80) && ((cpu->a ^ result) & 0x80));
			cpu->a = update_cmp_flags(cpu, result);
			break;
		}
		case 0xEB:	// *SBC
		case 0xE1:	// SBC
		case 0xE5:
		case 0xE9:
		case 0xED:
		case 0xF1:
		case 0xF5:
		case 0xF9:
		case 0xFD: {
			uint8_t operand = get_operand(cpu, instruction);
			uint16_t result = cpu->a - operand - !get_flag(cpu, CARRY);
			set_flag(cpu, CARRY, result <= 0xFF);
			set_flag(cpu, OVERFLOW, ((cpu->a ^ operand) & 0x80) && ((cpu->a ^ result) & 0x80));
			cpu->a = update_cmp_flags(cpu, result);
			break;
		}
		case 0xE6:	// INC
		case 0xEE:
		case 0xF6:
		case 0xFE: {
			uint16_t address = get_address(cpu, instruction);
			uint8_t value = read_ubyte(cpu->super, address) + 1;
			update_cmp_flags(cpu, value);
			write_ubyte(cpu->super, address, value);
			break;
		}
		case 0xE8:	// INX
			update_cmp_flags(cpu, ++cpu->x);
			break;
		case 0x1A:	// *NOP
		case 0x3A:
		case 0x5A:
		case 0x7A:
		case 0xDA:
		case 0xFA:
		case 0x80:	// *SKB
		case 0x82:
		case 0x89:
		case 0xC2:
		case 0xE2:
		case 0xEA:	// NOP
			break;
		case 0xF0:	// BEQ
			if (get_flag(cpu, ZERO))
				relative_jump(cpu, instruction);
			break;
		case 0xF8:	// SED
			set_flag(cpu, DECIMAL_MODE, 1);
			break;
		default:
			printf("Unsupported opcode: %02X (%s)\n", opcode, instruction->name);
			exit(-5);
	}

	cpu->pc += get_size(instruction);
	cpu->cycle += instruction->cycles;
	return (uint8_t)(cpu->cycle - start_cycle);
}
