#include <stdio.h>
#include <stdlib.h>
#include "ines.h"
#include "utils.h"

static rom_header_t read_rom_header(FILE *fp) {
	rom_header_t header = { };

	if (!fread(&header, sizeof(rom_header_t), 1, fp)) {
		printf("Reading the ROM header failed!\n");
        exit(-1);
	}

	if (header.magic[0] != 0x4E || header.magic[1] != 0x45 || header.magic[2] != 0x53 || header.magic[3] != 0x1A) {
		printf("Invalid ROM magic numbers! 0x%X, 0x%X, 0x%X, 0x%X\n", header.magic[0], header.magic[1], header.magic[2], header.magic[3]);
        exit(-1);
	}

	for (int i = 0; i < 5; ++i) {
		if (header.zero[i] != 0) {
			printf("Non-zero zero values in ROM header!\n");
		    exit(-1);
		}
	}

	return header;
}


static void read_rom_data(FILE *fp, uint8_t *target, size_t size) {
	if (size && !fread(target, 1, size, fp)) {
		printf("Reading ROM data (%lu bytes) failed!\n", size);
		exit(-1);
	}
}


static int has_trainer(rom_header_t *header) {
	return header->flags6 & 4;
}

static int has_play_choice_10(rom_header_t *header) {
	return header->flags6 & 2;
}


static size_t get_prg_size(rom_header_t *header) {
	return header->prg_rom_size * 16 * 1024;
}

static size_t get_chr_size(rom_header_t *header) {
	return header->chr_rom_size * 8 * 1024;
}


rom_t *read_rom(FILE *fp) {
	rom_header_t header = read_rom_header(fp);

	rom_t *rom = calloc(1, sizeof(rom_t) + get_prg_size(&header) + get_chr_size(&header));
	if (!rom) {
		printf("Allocating memory FAILED!\n");
		exit(-1);
	}

	rom->header = header;
	rom->prg_rom = (uint8_t*)(rom + 1);
	rom->chr_rom = (uint8_t*)(rom + 1) + get_prg_size(&header);

	read_rom_data(fp, &rom->trainer[0], has_trainer(&header) ? sizeof(rom->trainer) : 0);
	read_rom_data(fp, rom->prg_rom, get_prg_size(&header));
	read_rom_data(fp, rom->chr_rom, get_chr_size(&header));
	read_rom_data(fp, &rom->inst_rom[0], has_play_choice_10(&header) ? sizeof(rom->inst_rom) : 0);
	read_rom_data(fp, &rom->prom[0], has_play_choice_10(&header) ? sizeof(rom->prom) : 0);

	return rom;
}


void print_rom_header(rom_t *rom) {
	printf("  PRG ROM                 %d kB\n", rom->header.prg_rom_size * 16);
	if (rom->header.prg_ram_size)
		printf("  PRG RAM                 %d kB\n", rom->header.prg_ram_size * 8);

	if (rom->header.chr_rom_size)
		printf("  CHR ROM                 %d kB\n", rom->header.chr_rom_size * 8);
	else
		printf("  CHR RAM                 %d kB\n", 8);

	printf("  Mapper                  %d\n", (rom->header.flags7 & 0xF0) | (rom->header.flags6 >> 4));
	printf("  Mirroring               %s\n", rom->header.flags6 & 1 ? "vertical" : "horizontal");
	printf("  Persistent MEM          %s\n", rom->header.flags6 & 2 ? "yes" : "no");
	printf("  Trainer                 %s\n", has_trainer(&rom->header) ? "yes" : "no");
	printf("  4-screen VRAM           %s\n", rom->header.flags6 & 8 ? "yes" : "no");
	printf("  VS Unisystem            %s\n", rom->header.flags7 & 1 ? "yes" : "no");
	printf("  PlayChoice-10           %s\n", has_play_choice_10(&rom->header) ? "yes" : "no");
	printf("  NES 2.0                 %s\n", (rom->header.flags7 & 0x0C) == 0x08 ? "yes" : "no");
	printf("  TV system               %s\n",  rom->header.flags9 & 1 ? "PAL" : "NTSC");

	if (rom->header.flags10) {
		printf("  Flags 10                0x%02X (", rom->header.flags10);
		print_ubyte_as_binary(rom->header.flags10);
		printf(")\n");
	}
}
