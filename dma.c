#include "dma.h"

#include "mapper.h"


void dma_init(dma_t *dma) {
	dma->super.name = "DMA";
}

uint8_t dma_emulate(dma_t *dma) {
	if (dma->count == 0)
		return 0;

	mapper_t *mapper = dma->super.mapper;

	mapper->ppu.oam[dma->target++] = read_ubyte(mapper->cpu.super, dma->src++);
	dma->count--;
	return 2;
}
