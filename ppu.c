#include <assert.h>
#include <stdint.h>
#include <string.h>

#include "mapper.h"
#include "ppu.h"


#define SCAN_LINE_POST_RENDER 240
#define SCAN_LINE_VBLANK_0 241
#define SCAN_LINE_PRE_RENDER 261


enum {
	OAM_SPRITE_Y = 0,
	OAM_SPRITE_TILE = 1,
	OAM_SPRITE_ATTRIBUTES = 2,
	OAM_SPRITE_X = 3
};


enum registers {
	REG_PPUCTRL,  	// 0x2000
	REG_PPUMASK,    // 0x2001
	REG_PPUSTATUS,	// 0x2002
	REG_OAMADDR,  	// 0x2003
	REG_OAMDATA,  	// 0x2004
	REG_PPUSCROLL,	// 0x2005
	REG_PPUADDR,  	// 0x2006
	REG_PPUDATA,  	// 0x2007
	REG_OAMDMA    	// 0x4014
};


typedef enum {
	SPRITE_OVERFLOW = 5,
	SPRITE_ZERO_HIT = 6,
	VERTICAL_BLANK = 7
} ppu_flag_t;

typedef struct {
	uint8_t color;
	uint8_t priority;
	uint8_t index;
} sprite_pixel_t;


static void set_flag(ppu_t *ppu, ppu_flag_t flag, uint8_t value) {
	ppu->reg_ppustatus ^= (-!!value ^ ppu->reg_ppustatus) & (1 << flag);
}


static void set_t_namespace(ppu_t *ppu, uint8_t value) {
	ppu->t = (ppu->t & 0x73FF) | ((value & 0x03) << 10);
}



void ppu_write_ubyte(void *mapper, uint16_t address, uint8_t value) {
	mapper_t *m = (mapper_t*)mapper;
	ppu_t *ppu = &m->ppu;

	switch (address) {
		case REG_PPUCTRL:
			set_t_namespace(ppu, value);
			ppu->reg_ppuctrl = value;
			if (value & ppu->reg_ppustatus & 0x80)
				m->cpu.nmi = 1;
			break;
		case REG_PPUMASK:
			ppu->reg_ppumask = value;
			break;
		case REG_OAMADDR:
			ppu->reg_oamaddr = value;
			break;
		case REG_OAMDATA:
			if ((ppu->scan_line > SCAN_LINE_POST_RENDER && ppu->scan_line < SCAN_LINE_PRE_RENDER) || !(ppu->reg_ppumask & 0x18))
				ppu->oam[ppu->reg_oamaddr++] = value;
			break;
		case REG_PPUSCROLL:
			if ((ppu->w = !ppu->w)) {
				ppu->t = (ppu->t & 0x7FE0) | value >> 3;
				ppu->x = value & 0x07;
			}
			else {
				ppu->t = (ppu->t & 0x1C1F) | ((value & 0x07) << 12) | ((value & 0xF8) << 2);
			}
			break;
		case REG_PPUADDR:
			if ((ppu->w = !ppu->w))
				ppu->t = (ppu->t & 0x00FF) | ((value & 0x3F) << 8);
			else
				ppu->v = ppu->t = (ppu->t & 0xFF00) | value;
			break;
		case REG_PPUDATA:
			write_ubyte(ppu->super, ppu->v, value);
			ppu->v += (ppu->reg_ppuctrl & 0x04) ? 32 : 1;
			break;
		case REG_OAMDMA:
			m->dma.count = 0x100;
			m->dma.src = ((uint16_t)value) << 8;
			m->dma.target = ppu->reg_oamaddr;
			if (m->cpu.cycle++ & 1)
				m->cpu.cycle++;
			break;
		default:
			printf("Unimplemented PPU register write at local address %02X\n", address);
			break;
	}

	ppu->gen_latch = value;
}


uint8_t ppu_read_ubyte(void *mapper, uint16_t address) {
	assert(address <= REG_OAMDMA);
	ppu_t *ppu = &((mapper_t*)mapper)->ppu;

	uint8_t result;

	switch (address) {
		case REG_PPUSTATUS:
			result = (ppu->gen_latch & 0x1F) | ppu->reg_ppustatus;
			set_flag(ppu, VERTICAL_BLANK, 0);
			ppu->w = 0;
			break;
		case REG_OAMDATA:
			if (ppu->scan_line < SCAN_LINE_POST_RENDER && ppu->scan_line_cycle - 1 < 64 && (ppu->reg_ppumask & 0x18))
				result = 0xFF;
			else
				result = ppu->oam[ppu->reg_oamaddr];
			break;
		case REG_PPUDATA:
			if (ppu->v >= 0x3F00) {
				result = read_ubyte(ppu->super, ppu->v);
				ppu->reg_ppudata = read_ubyte(ppu->super, ppu->v - 0x1000);
				if (ppu->reg_ppumask & 1)
					ppu->reg_ppudata &= 0x30;
			}
			else {
				result = ppu->reg_ppudata;
				ppu->reg_ppudata = read_ubyte(ppu->super, ppu->v);
			}
			ppu->v += (ppu->reg_ppuctrl & 0x04) ? 32 : 1;
			break;
		default:
			result = ppu->gen_latch;
			break;
	}

	return result;
}


static int ppu_update_scan_line_cycle(ppu_t *ppu, uint8_t rendering_enabled) {
	if (++ppu->scan_line_cycle < 341)
		return 0;

	ppu->scan_line_cycle = 0;
	if (ppu->scan_line++ == SCAN_LINE_PRE_RENDER) {
		ppu->scan_line = 0;
		ppu->frame++;
		if (rendering_enabled && (ppu->frame & 1))
			++ppu->scan_line_cycle;
		return 1;
	}

	return 0;
}


static void increment_coarse_x(ppu_t *ppu) {
	if ((ppu->v & 0x001F) != 0x001F) {
		++ppu->v;
		return;
	}

	ppu->v &= ~0x001F;
	ppu->v ^= 0x0400;
}


static void increment_y(ppu_t *ppu) {
	if ((ppu->v & 0x7000) != 0x7000) {
		ppu->v += 0x1000;
		return;
	}

	ppu->v &= ~0x7000;
	uint16_t coarse_y = (ppu->v & 0x03E0) >> 5;
	if (coarse_y == 29) {		// Switch nametable
		coarse_y = 0;
		ppu->v ^= 0x0800;
	}
	else if (coarse_y == 31) {	// Overflow
		coarse_y = 0;
	}
	else {
		coarse_y += 1;
	}

	ppu->v = (ppu->v & ~0x03E0) | (coarse_y << 5);
}


static uint8_t fetch_sprite_pixel(ppu_t *ppu, uint8_t tile, uint8_t y, uint8_t x, uint8_t attributes) {
	int tall_sprites = ppu->reg_ppuctrl & 0x20;

	if (attributes & 0x40)
		x = 7 - x;
	if (attributes & 0x80)
		y = (tall_sprites ? 15 : 7) - y;

	uint16_t address;
	if (tall_sprites) {
		address = (tile & 1) << 12;
		if (y >= 8) {
			tile = tile | 1;
			y -= 8;
		}
		else {
			tile = tile & 0xFE;
		}
	}
	else {
		address = ((ppu->reg_ppuctrl & 0x08) << 9);
	}

	address |= (tile << 4) | y;

	uint8_t high_bit = (read_ubyte(ppu->super, address | 0x8) >> x) & 1;
	uint8_t low_bit = (read_ubyte(ppu->super, address) >> x) & 1;

	return high_bit + high_bit + low_bit;
}


static uint8_t fetch_bg_tile_data(ppu_t *ppu, uint16_t tile, uint16_t plane) {
	uint16_t address = ((ppu->reg_ppuctrl & 0x10) << 8) | (tile << 4) | (ppu->v >> 12);
	if (plane)
		address |= 0x8;

	return read_ubyte(ppu->super, address);
}


static uint8_t fetch_attribute_data(ppu_t *ppu) {
	uint16_t address = 0x23C0 | (ppu->v & 0x0C00) | ((ppu->v >> 4) & 0x38) | ((ppu->v >> 2) & 0x07);
	uint8_t result = read_ubyte(ppu->super, address);

	if (ppu->v & 0x40) {
		if (ppu->v & 0x02)
			result >>= 6;
		else
			result >>= 4;
	}
	else if ((ppu->v & 0x02)) {
		result >>= 2;
	}

	return result & 0x03;
}


static void handle_sprite_pre_fetches(ppu_t *ppu) {
	uint16_t cycle = ppu->scan_line_cycle;
	if (cycle == 0)
		return;

	if (cycle == 64)
		memset(&ppu->oam_secondary[0], 0xFF, 32);

	if (cycle == 257) {
		uint16_t scan_line = ppu->scan_line;

		int n = 0;
		int found = 0;

		int sprite_height = ppu->reg_ppuctrl & 0x20 ? 16 : 8;

		do {
			uint16_t sprite_y = ppu->oam[n * 4 + OAM_SPRITE_Y];
			ppu->oam_secondary[found * 4 + OAM_SPRITE_Y] = sprite_y;
			if (sprite_y <= scan_line && sprite_y + sprite_height > scan_line) {
				if (found == 0)
					ppu->sprite_0_visible = n == 0;

				ppu->oam_secondary[found * 4 + OAM_SPRITE_TILE] = ppu->oam[n * 4 + OAM_SPRITE_TILE];
				ppu->oam_secondary[found * 4 + OAM_SPRITE_ATTRIBUTES] = ppu->oam[n * 4 + OAM_SPRITE_ATTRIBUTES];
				ppu->oam_secondary[found * 4 + OAM_SPRITE_X] = ppu->oam[n * 4 + OAM_SPRITE_X];
				++found;
			}
		} while (found < 8 && ++n < 64);

		int m = 0;
		while (n < 64) {
			uint16_t sprite_y = ppu->oam[n * 4 + m];
			if (sprite_y < scan_line && sprite_y + 8 >= scan_line)
				set_flag(ppu, SPRITE_OVERFLOW, 1);
			else
				m = (m + 1) % 4;
			++n;
		}
	}

	if (cycle == 320)
		memcpy(&ppu->oam_current[0], &ppu->oam_secondary[0], 32);
}


static void handle_bg_pre_fetches(ppu_t *ppu) {
	uint16_t cycle = ppu->scan_line_cycle;

	if (cycle == 0)
		return;

	if (cycle <= 256 || cycle >= 321) {
		if (cycle >= 2 && cycle <= 337) {
			ppu->at_low_shift = (ppu->at_low_shift << 1) | ppu->at_low_latch;
			ppu->at_high_shift = (ppu->at_high_shift << 1) | ppu->at_high_latch;
			ppu->bg_tile_high_shift <<= 1;
			ppu->bg_tile_low_shift <<= 1;
		}

		switch ((cycle - 1) & 0x07) {
			case 0:
				ppu->at_low_latch = ppu->at & 1;
				ppu->at_high_latch = (ppu->at >> 1) & 1;
				ppu->bg_tile_low_shift |= ppu->bg_tile_low_latch;
				ppu->bg_tile_high_shift |= ppu->bg_tile_high_latch;
				uint16_t tile = read_ubyte(ppu->super, 0x2000 | (ppu->v & 0x0FFF));
				uint16_t low_temp = fetch_bg_tile_data(ppu, tile, 0);
				uint16_t high_temp = fetch_bg_tile_data(ppu, tile, 1);
				if (cycle < 337) {
					ppu->bg_tile_low_latch = low_temp;
					ppu->bg_tile_high_latch = high_temp;
				}
				break;
			case 3:
				if (cycle < 337)
					ppu->at = fetch_attribute_data(ppu);
				break;
			case 7:
				increment_coarse_x(ppu);
				break;
			default:
				break;
		}
	}

	if (cycle == 256)
		increment_y(ppu);
	if (cycle == 257)
		ppu->v = (ppu->v & ~0x041F) | (ppu->t & 0x041F);
}


static sprite_pixel_t get_sprite_pixel(ppu_t *ppu, uint16_t cycle) {
	sprite_pixel_t result = { .priority = 1, .index = 0xFF };

	if (ppu->scan_line && (ppu->reg_ppumask & 0x10)) {
		for (uint8_t i = 0; i < 8; ++i) {
			 uint16_t x = ppu->oam_current[i * 4 + OAM_SPRITE_X];
			 if (x < cycle && x + 8 >= cycle) {
				 uint8_t attributes = ppu->oam_current[i * 4 + OAM_SPRITE_ATTRIBUTES];
				 uint8_t sprite_x = 8 + x - cycle;
				 uint8_t sprite_y = ppu->scan_line - 1 - ppu->oam_current[i * 4 + OAM_SPRITE_Y];

				 uint8_t color = fetch_sprite_pixel(ppu, ppu->oam_current[i * 4 + OAM_SPRITE_TILE], sprite_y, sprite_x, attributes);
				 if (!color)
					 continue;

				 result.color = ((attributes & 0x03) << 2) | color;
				 result.priority = attributes & 0x20;
				 result.index = i;
				 break;
			 }
		}
	}

	return result;
}


static uint8_t get_bg_color(ppu_t *ppu) {
	if (!(ppu->reg_ppumask & 0x08))
		return 0;

	uint16_t x = 0x01 << (15 - ppu->x);
	return ((ppu->bg_tile_high_shift & x) ? 2 : 0) | ((ppu->bg_tile_low_shift & x) ? 1 : 0);
}



static void render_visible_line(ppu_t *ppu) {
	uint16_t cycle = ppu->scan_line_cycle;

	if (cycle >= 1 && cycle <= 256) {
		sprite_pixel_t sprite_pixel = get_sprite_pixel(ppu, cycle);
		uint8_t bg_color = get_bg_color(ppu);

		if (bg_color && sprite_pixel.color && sprite_pixel.index == 0 && ppu->sprite_0_visible)
			set_flag(ppu, SPRITE_ZERO_HIT, 1);

		uint16_t palette = 0x3F00;
		if (bg_color && (sprite_pixel.priority || !sprite_pixel.color)) {
			palette |= (((ppu->at_high_shift >> (7 - ppu->x)) & 1) << 3) | (((ppu->at_low_shift >> (7 - ppu->x)) & 1) << 2) | bg_color;
		}
		else {
			palette |= 0x10 | sprite_pixel.color;
		}

		uint8_t color = read_ubyte(ppu->super, palette);
		if (ppu->reg_ppumask & 1)
			color &= 0x30;

		ppu->picture[(ppu->scan_line << 8) | (cycle - 1)] = color;
	}
}


static void handle_vblank_0(ppu_t *ppu) {
	if (ppu->scan_line_cycle == 0) {
		ppu->vblank_set = 1;
	}
	else if (ppu->scan_line_cycle == 1 && ppu->vblank_set) {
		set_flag(ppu, VERTICAL_BLANK, 1);
		if (ppu->reg_ppuctrl & 0x80)
			((mapper_t*)ppu->super.mapper)->cpu.nmi = 1;
	}
}



int ppu_emulate(ppu_t *ppu) {
	int rendering_enabled = ppu->reg_ppumask & 0x18;
	uint16_t scan_line = ppu->scan_line;

	if (scan_line < SCAN_LINE_POST_RENDER) {    	// Visible lines
		if (rendering_enabled) {
			handle_bg_pre_fetches(ppu);
			handle_sprite_pre_fetches(ppu);
		}
		render_visible_line(ppu);
	}
	else if (scan_line < SCAN_LINE_PRE_RENDER) {	// Post-render line & vertical sync lines
		if (scan_line == SCAN_LINE_VBLANK_0)
			handle_vblank_0(ppu);
	}
	else {                                      	// Pre-render line
		if (ppu->scan_line_cycle == 1)
			ppu->reg_ppustatus = 0x00;				// Reset vertical blank, sprite-0, overflow
		if (rendering_enabled) {
			handle_bg_pre_fetches(ppu);
			if (ppu->scan_line_cycle >= 280 && ppu->scan_line_cycle <= 304)
				ppu->v = (ppu->v & ~0x7BE0) | (ppu->t & 0x7BE0);	// Reset vertical v bits from t
		}
	}

	return ppu_update_scan_line_cycle(ppu, rendering_enabled);
}


void ppu_init(ppu_t *ppu) {
	ppu->super.name = "PPU";
	ppu->reg_ppuctrl = 0x00;
	ppu->reg_ppumask = 0x00;
	ppu->reg_ppustatus = 0x00;
	ppu->scan_line = 0;
}
